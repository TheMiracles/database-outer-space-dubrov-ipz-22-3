﻿namespace outer_space_database
{
    partial class AddComets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddComets));
            this.label25 = new System.Windows.Forms.Label();
            this.textBox_IDPROSTIR_Comets = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_Color_Comets = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox_Radius_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Mass_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Name_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Id_Comets = new System.Windows.Forms.TextBox();
            this.btnAddComets = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(167, 279);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 16);
            this.label25.TabIndex = 35;
            this.label25.Text = "ID простір:";
            // 
            // textBox_IDPROSTIR_Comets
            // 
            this.textBox_IDPROSTIR_Comets.Location = new System.Drawing.Point(249, 273);
            this.textBox_IDPROSTIR_Comets.Name = "textBox_IDPROSTIR_Comets";
            this.textBox_IDPROSTIR_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_IDPROSTIR_Comets.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(197, 248);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 16);
            this.label20.TabIndex = 33;
            this.label20.Text = "Колір:";
            // 
            // textBox_Color_Comets
            // 
            this.textBox_Color_Comets.Location = new System.Drawing.Point(249, 245);
            this.textBox_Color_Comets.Name = "textBox_Color_Comets";
            this.textBox_Color_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_Comets.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(190, 223);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 16);
            this.label21.TabIndex = 31;
            this.label21.Text = "Радіус:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(199, 192);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 16);
            this.label22.TabIndex = 30;
            this.label22.Text = "Маса:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(190, 164);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 16);
            this.label24.TabIndex = 29;
            this.label24.Text = "Назва:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(219, 136);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(23, 16);
            this.label23.TabIndex = 24;
            this.label23.Text = "ID:";
            // 
            // textBox_Radius_Comets
            // 
            this.textBox_Radius_Comets.Location = new System.Drawing.Point(249, 217);
            this.textBox_Radius_Comets.Name = "textBox_Radius_Comets";
            this.textBox_Radius_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_Comets.TabIndex = 28;
            // 
            // textBox_Mass_Comets
            // 
            this.textBox_Mass_Comets.Location = new System.Drawing.Point(249, 189);
            this.textBox_Mass_Comets.Name = "textBox_Mass_Comets";
            this.textBox_Mass_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_Comets.TabIndex = 27;
            // 
            // textBox_Name_Comets
            // 
            this.textBox_Name_Comets.Location = new System.Drawing.Point(249, 161);
            this.textBox_Name_Comets.Name = "textBox_Name_Comets";
            this.textBox_Name_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_Comets.TabIndex = 26;
            // 
            // textBox_Id_Comets
            // 
            this.textBox_Id_Comets.Location = new System.Drawing.Point(249, 133);
            this.textBox_Id_Comets.Name = "textBox_Id_Comets";
            this.textBox_Id_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Id_Comets.TabIndex = 25;
            // 
            // btnAddComets
            // 
            this.btnAddComets.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddComets.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddComets.Location = new System.Drawing.Point(249, 301);
            this.btnAddComets.Name = "btnAddComets";
            this.btnAddComets.Size = new System.Drawing.Size(196, 48);
            this.btnAddComets.TabIndex = 54;
            this.btnAddComets.Text = "Зберегти ";
            this.btnAddComets.UseVisualStyleBackColor = false;
            this.btnAddComets.Click += new System.EventHandler(this.btnAddComets_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(483, 136);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(286, 151);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            // 
            // AddComets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAddComets);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.textBox_IDPROSTIR_Comets);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox_Color_Comets);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.textBox_Radius_Comets);
            this.Controls.Add(this.textBox_Mass_Comets);
            this.Controls.Add(this.textBox_Name_Comets);
            this.Controls.Add(this.textBox_Id_Comets);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddComets";
            this.Text = "Комети";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox_IDPROSTIR_Comets;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox_Color_Comets;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox_Radius_Comets;
        private System.Windows.Forms.TextBox textBox_Mass_Comets;
        private System.Windows.Forms.TextBox textBox_Name_Comets;
        private System.Windows.Forms.TextBox textBox_Id_Comets;
        private System.Windows.Forms.Button btnAddComets;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}