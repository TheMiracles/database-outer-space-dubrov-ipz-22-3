﻿namespace outer_space_database
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TabControl = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox_IDPROSTIR_OuterSpace = new System.Windows.Forms.TextBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox_Color_OuterSpace = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox_Radius_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_Mass_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_Name_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_SunnySpace_id_OuterSpace = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnChangeOuterSpace = new System.Windows.Forms.Button();
            this.btnUpdateOuterSpace = new System.Windows.Forms.Button();
            this.btnDeleteOuterSpace = new System.Windows.Forms.Button();
            this.btnNewOuterSpace = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.textBox_SearchOuterSpace = new System.Windows.Forms.TextBox();
            this.btnRefresh_OuterSpace = new System.Windows.Forms.Button();
            this.btnClearOuterSpace = new System.Windows.Forms.Button();
            this.dataGridViewOuterSpace = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnChangeStar = new System.Windows.Forms.Button();
            this.btnUpdateStar = new System.Windows.Forms.Button();
            this.btnDeleteStar = new System.Windows.Forms.Button();
            this.btnNewStar = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_Color_Star = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox_Radius_Star = new System.Windows.Forms.TextBox();
            this.textBox_Mass_Star = new System.Windows.Forms.TextBox();
            this.textBox_Name_Star = new System.Windows.Forms.TextBox();
            this.textBox_Id_Star = new System.Windows.Forms.TextBox();
            this.dataGridViewStars = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.textBox_SearchStars = new System.Windows.Forms.TextBox();
            this.btnRefresh_Stars = new System.Windows.Forms.Button();
            this.btnClearStars = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_Satellite_id = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Sun_id = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_Color = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Type_planet = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Distance_Sun = new System.Windows.Forms.TextBox();
            this.textBox_Radius = new System.Windows.Forms.TextBox();
            this.textBox_Mass = new System.Windows.Forms.TextBox();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.textBox_id = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnChangeSatellite = new System.Windows.Forms.Button();
            this.btnUpdateSatellites = new System.Windows.Forms.Button();
            this.btnDeleteSatellite = new System.Windows.Forms.Button();
            this.btnNewSatellite = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_Satallite_Color = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox_Satallite_Radius = new System.Windows.Forms.TextBox();
            this.textBox_Satallite_Mass = new System.Windows.Forms.TextBox();
            this.textBox_Satallite_Name = new System.Windows.Forms.TextBox();
            this.textBox_Satallite_id = new System.Windows.Forms.TextBox();
            this.dataGridViewSatellites = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textBox_SearchSatellites = new System.Windows.Forms.TextBox();
            this.btnRefresh_Satallites = new System.Windows.Forms.Button();
            this.btnClearSatellites = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox_IDPROSTIR_Meteorite = new System.Windows.Forms.TextBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox_Color_Meteorite = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox_Radius_Meteorite = new System.Windows.Forms.TextBox();
            this.textBox_Mass_Meteorite = new System.Windows.Forms.TextBox();
            this.textBox_Name_Meteorite = new System.Windows.Forms.TextBox();
            this.textBox_Id_Meteorite = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnChangeMeteorite = new System.Windows.Forms.Button();
            this.btnUpdateMeteorite = new System.Windows.Forms.Button();
            this.btnDeleteMeteorite = new System.Windows.Forms.Button();
            this.btnNewMeteorite = new System.Windows.Forms.Button();
            this.dataGridViewMeteorites = new System.Windows.Forms.DataGridView();
            this.panel15 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.textBox_SearchMeteorites = new System.Windows.Forms.TextBox();
            this.btnRefresh_Meteorites = new System.Windows.Forms.Button();
            this.btnClearMeteorite = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox_IDPROSTIR_Comets = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_Color_Comets = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox_Radius_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Mass_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Name_Comets = new System.Windows.Forms.TextBox();
            this.textBox_Id_Comets = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnChangeComets = new System.Windows.Forms.Button();
            this.btnUpdateComets = new System.Windows.Forms.Button();
            this.btnDeleteComets = new System.Windows.Forms.Button();
            this.btnNewComets = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.textBox_SearchComets = new System.Windows.Forms.TextBox();
            this.btnRefresh_Comets = new System.Windows.Forms.Button();
            this.btnClearComets = new System.Windows.Forms.Button();
            this.dataGridViewComets = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.CreateBackup = new System.Windows.Forms.Button();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.керуванняToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripUser = new System.Windows.Forms.ToolStripTextBox();
            this.TabControl.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOuterSpace)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStars)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSatellites)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMeteorites)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComets)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.TabControl.Controls.Add(this.tabPage6);
            this.TabControl.Controls.Add(this.tabPage3);
            this.TabControl.Controls.Add(this.tabPage1);
            this.TabControl.Controls.Add(this.tabPage2);
            this.TabControl.Controls.Add(this.tabPage5);
            this.TabControl.Controls.Add(this.tabPage4);
            this.TabControl.Controls.Add(this.tabPage7);
            this.TabControl.Location = new System.Drawing.Point(2, 34);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(874, 564);
            this.TabControl.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel16);
            this.tabPage6.Controls.Add(this.panel17);
            this.tabPage6.Controls.Add(this.panel18);
            this.tabPage6.Controls.Add(this.dataGridViewOuterSpace);
            this.tabPage6.Location = new System.Drawing.Point(4, 28);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(866, 532);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "Космічний простір";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label32);
            this.panel16.Controls.Add(this.textBox_IDPROSTIR_OuterSpace);
            this.panel16.Controls.Add(this.pictureBox16);
            this.panel16.Controls.Add(this.label33);
            this.panel16.Controls.Add(this.textBox_Color_OuterSpace);
            this.panel16.Controls.Add(this.label34);
            this.panel16.Controls.Add(this.label35);
            this.panel16.Controls.Add(this.label36);
            this.panel16.Controls.Add(this.label37);
            this.panel16.Controls.Add(this.textBox_Radius_OuterSpace);
            this.panel16.Controls.Add(this.textBox_Mass_OuterSpace);
            this.panel16.Controls.Add(this.textBox_Name_OuterSpace);
            this.panel16.Controls.Add(this.textBox_SunnySpace_id_OuterSpace);
            this.panel16.Location = new System.Drawing.Point(20, 292);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(666, 224);
            this.panel16.TabIndex = 27;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 189);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 16);
            this.label32.TabIndex = 23;
            this.label32.Text = "ID простір:";
            // 
            // textBox_IDPROSTIR_OuterSpace
            // 
            this.textBox_IDPROSTIR_OuterSpace.Location = new System.Drawing.Point(85, 183);
            this.textBox_IDPROSTIR_OuterSpace.Name = "textBox_IDPROSTIR_OuterSpace";
            this.textBox_IDPROSTIR_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_IDPROSTIR_OuterSpace.TabIndex = 22;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox16.BackgroundImage")));
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox16.Location = new System.Drawing.Point(312, 15);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(354, 204);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 21;
            this.pictureBox16.TabStop = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(33, 158);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 16);
            this.label33.TabIndex = 15;
            this.label33.Text = "Колір:";
            // 
            // textBox_Color_OuterSpace
            // 
            this.textBox_Color_OuterSpace.Location = new System.Drawing.Point(85, 155);
            this.textBox_Color_OuterSpace.Name = "textBox_Color_OuterSpace";
            this.textBox_Color_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_OuterSpace.TabIndex = 14;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(26, 133);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 16);
            this.label34.TabIndex = 10;
            this.label34.Text = "Радіус:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(35, 102);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 16);
            this.label35.TabIndex = 9;
            this.label35.Text = "Маса:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(26, 74);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 16);
            this.label36.TabIndex = 8;
            this.label36.Text = "Назва:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(55, 46);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(23, 16);
            this.label37.TabIndex = 3;
            this.label37.Text = "ID:";
            // 
            // textBox_Radius_OuterSpace
            // 
            this.textBox_Radius_OuterSpace.Location = new System.Drawing.Point(85, 127);
            this.textBox_Radius_OuterSpace.Name = "textBox_Radius_OuterSpace";
            this.textBox_Radius_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_OuterSpace.TabIndex = 6;
            // 
            // textBox_Mass_OuterSpace
            // 
            this.textBox_Mass_OuterSpace.Location = new System.Drawing.Point(85, 99);
            this.textBox_Mass_OuterSpace.Name = "textBox_Mass_OuterSpace";
            this.textBox_Mass_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_OuterSpace.TabIndex = 5;
            // 
            // textBox_Name_OuterSpace
            // 
            this.textBox_Name_OuterSpace.Location = new System.Drawing.Point(85, 71);
            this.textBox_Name_OuterSpace.Name = "textBox_Name_OuterSpace";
            this.textBox_Name_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_OuterSpace.TabIndex = 4;
            // 
            // textBox_SunnySpace_id_OuterSpace
            // 
            this.textBox_SunnySpace_id_OuterSpace.Location = new System.Drawing.Point(85, 43);
            this.textBox_SunnySpace_id_OuterSpace.Name = "textBox_SunnySpace_id_OuterSpace";
            this.textBox_SunnySpace_id_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_SunnySpace_id_OuterSpace.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.btnChangeOuterSpace);
            this.panel17.Controls.Add(this.btnUpdateOuterSpace);
            this.panel17.Controls.Add(this.btnDeleteOuterSpace);
            this.panel17.Controls.Add(this.btnNewOuterSpace);
            this.panel17.Location = new System.Drawing.Point(692, 292);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(167, 224);
            this.panel17.TabIndex = 28;
            // 
            // btnChangeOuterSpace
            // 
            this.btnChangeOuterSpace.BackColor = System.Drawing.Color.White;
            this.btnChangeOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeOuterSpace.Location = new System.Drawing.Point(17, 123);
            this.btnChangeOuterSpace.Name = "btnChangeOuterSpace";
            this.btnChangeOuterSpace.Size = new System.Drawing.Size(138, 36);
            this.btnChangeOuterSpace.TabIndex = 3;
            this.btnChangeOuterSpace.Text = "Змінити";
            this.btnChangeOuterSpace.UseVisualStyleBackColor = false;
            this.btnChangeOuterSpace.Click += new System.EventHandler(this.btnChangeOuterSpace_Click);
            // 
            // btnUpdateOuterSpace
            // 
            this.btnUpdateOuterSpace.BackColor = System.Drawing.Color.White;
            this.btnUpdateOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateOuterSpace.Location = new System.Drawing.Point(17, 169);
            this.btnUpdateOuterSpace.Name = "btnUpdateOuterSpace";
            this.btnUpdateOuterSpace.Size = new System.Drawing.Size(138, 36);
            this.btnUpdateOuterSpace.TabIndex = 2;
            this.btnUpdateOuterSpace.Text = "Зберегти";
            this.btnUpdateOuterSpace.UseVisualStyleBackColor = false;
            this.btnUpdateOuterSpace.Click += new System.EventHandler(this.btnUpdateOuterSpace_Click);
            // 
            // btnDeleteOuterSpace
            // 
            this.btnDeleteOuterSpace.BackColor = System.Drawing.Color.White;
            this.btnDeleteOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteOuterSpace.Location = new System.Drawing.Point(17, 77);
            this.btnDeleteOuterSpace.Name = "btnDeleteOuterSpace";
            this.btnDeleteOuterSpace.Size = new System.Drawing.Size(138, 36);
            this.btnDeleteOuterSpace.TabIndex = 1;
            this.btnDeleteOuterSpace.Text = "Видалити";
            this.btnDeleteOuterSpace.UseVisualStyleBackColor = false;
            this.btnDeleteOuterSpace.Click += new System.EventHandler(this.btnDeleteOuterSpace_Click);
            // 
            // btnNewOuterSpace
            // 
            this.btnNewOuterSpace.BackColor = System.Drawing.Color.White;
            this.btnNewOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewOuterSpace.Location = new System.Drawing.Point(17, 29);
            this.btnNewOuterSpace.Name = "btnNewOuterSpace";
            this.btnNewOuterSpace.Size = new System.Drawing.Size(138, 36);
            this.btnNewOuterSpace.TabIndex = 0;
            this.btnNewOuterSpace.Text = "Новий запис";
            this.btnNewOuterSpace.UseVisualStyleBackColor = false;
            this.btnNewOuterSpace.Click += new System.EventHandler(this.btnNewOuterSpace_Click);
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.pictureBox18);
            this.panel18.Controls.Add(this.pictureBox17);
            this.panel18.Controls.Add(this.textBox_SearchOuterSpace);
            this.panel18.Controls.Add(this.btnRefresh_OuterSpace);
            this.panel18.Controls.Add(this.btnClearOuterSpace);
            this.panel18.Location = new System.Drawing.Point(7, 4);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(852, 62);
            this.panel18.TabIndex = 25;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox18.BackgroundImage")));
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox18.Location = new System.Drawing.Point(286, 12);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(55, 47);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 15;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox17.BackgroundImage")));
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox17.Location = new System.Drawing.Point(3, -5);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(291, 75);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 14;
            this.pictureBox17.TabStop = false;
            // 
            // textBox_SearchOuterSpace
            // 
            this.textBox_SearchOuterSpace.Location = new System.Drawing.Point(415, 12);
            this.textBox_SearchOuterSpace.Multiline = true;
            this.textBox_SearchOuterSpace.Name = "textBox_SearchOuterSpace";
            this.textBox_SearchOuterSpace.Size = new System.Drawing.Size(238, 37);
            this.textBox_SearchOuterSpace.TabIndex = 1;
            this.textBox_SearchOuterSpace.TextChanged += new System.EventHandler(this.textBox_SearchOuterSpace_TextChanged);
            // 
            // btnRefresh_OuterSpace
            // 
            this.btnRefresh_OuterSpace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh_OuterSpace.BackgroundImage")));
            this.btnRefresh_OuterSpace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh_OuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh_OuterSpace.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh_OuterSpace.Name = "btnRefresh_OuterSpace";
            this.btnRefresh_OuterSpace.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh_OuterSpace.TabIndex = 0;
            this.btnRefresh_OuterSpace.UseVisualStyleBackColor = true;
            this.btnRefresh_OuterSpace.Click += new System.EventHandler(this.btnRefresh_OuterSpace_Click);
            // 
            // btnClearOuterSpace
            // 
            this.btnClearOuterSpace.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearOuterSpace.BackgroundImage")));
            this.btnClearOuterSpace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClearOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearOuterSpace.Location = new System.Drawing.Point(768, 3);
            this.btnClearOuterSpace.Name = "btnClearOuterSpace";
            this.btnClearOuterSpace.Size = new System.Drawing.Size(50, 50);
            this.btnClearOuterSpace.TabIndex = 0;
            this.btnClearOuterSpace.UseVisualStyleBackColor = true;
            this.btnClearOuterSpace.Click += new System.EventHandler(this.btnClearOuterSpace_Click);
            // 
            // dataGridViewOuterSpace
            // 
            this.dataGridViewOuterSpace.AllowUserToAddRows = false;
            this.dataGridViewOuterSpace.AllowUserToDeleteRows = false;
            this.dataGridViewOuterSpace.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewOuterSpace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOuterSpace.Location = new System.Drawing.Point(9, 72);
            this.dataGridViewOuterSpace.Name = "dataGridViewOuterSpace";
            this.dataGridViewOuterSpace.ReadOnly = true;
            this.dataGridViewOuterSpace.RowHeadersWidth = 51;
            this.dataGridViewOuterSpace.RowTemplate.Height = 24;
            this.dataGridViewOuterSpace.Size = new System.Drawing.Size(850, 214);
            this.dataGridViewOuterSpace.TabIndex = 26;
            this.dataGridViewOuterSpace.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOuterSpace_CellClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.panel8);
            this.tabPage3.Controls.Add(this.dataGridViewStars);
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Location = new System.Drawing.Point(4, 28);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(866, 532);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Зірки";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnChangeStar);
            this.panel7.Controls.Add(this.btnUpdateStar);
            this.panel7.Controls.Add(this.btnDeleteStar);
            this.panel7.Controls.Add(this.btnNewStar);
            this.panel7.Location = new System.Drawing.Point(692, 292);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(167, 224);
            this.panel7.TabIndex = 20;
            // 
            // btnChangeStar
            // 
            this.btnChangeStar.BackColor = System.Drawing.Color.White;
            this.btnChangeStar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeStar.Location = new System.Drawing.Point(17, 123);
            this.btnChangeStar.Name = "btnChangeStar";
            this.btnChangeStar.Size = new System.Drawing.Size(138, 36);
            this.btnChangeStar.TabIndex = 3;
            this.btnChangeStar.Text = "Змінити";
            this.btnChangeStar.UseVisualStyleBackColor = false;
            this.btnChangeStar.Click += new System.EventHandler(this.btnChangeStar_Click);
            // 
            // btnUpdateStar
            // 
            this.btnUpdateStar.BackColor = System.Drawing.Color.White;
            this.btnUpdateStar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateStar.Location = new System.Drawing.Point(17, 169);
            this.btnUpdateStar.Name = "btnUpdateStar";
            this.btnUpdateStar.Size = new System.Drawing.Size(138, 36);
            this.btnUpdateStar.TabIndex = 2;
            this.btnUpdateStar.Text = "Зберегти";
            this.btnUpdateStar.UseVisualStyleBackColor = false;
            this.btnUpdateStar.Click += new System.EventHandler(this.btnUpdateStar_Click);
            // 
            // btnDeleteStar
            // 
            this.btnDeleteStar.BackColor = System.Drawing.Color.White;
            this.btnDeleteStar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteStar.Location = new System.Drawing.Point(17, 77);
            this.btnDeleteStar.Name = "btnDeleteStar";
            this.btnDeleteStar.Size = new System.Drawing.Size(138, 36);
            this.btnDeleteStar.TabIndex = 1;
            this.btnDeleteStar.Text = "Видалити";
            this.btnDeleteStar.UseVisualStyleBackColor = false;
            this.btnDeleteStar.Click += new System.EventHandler(this.btnDeleteStar_Click);
            // 
            // btnNewStar
            // 
            this.btnNewStar.BackColor = System.Drawing.Color.White;
            this.btnNewStar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewStar.Location = new System.Drawing.Point(17, 29);
            this.btnNewStar.Name = "btnNewStar";
            this.btnNewStar.Size = new System.Drawing.Size(138, 36);
            this.btnNewStar.TabIndex = 0;
            this.btnNewStar.Text = "Новий запис";
            this.btnNewStar.UseVisualStyleBackColor = false;
            this.btnNewStar.Click += new System.EventHandler(this.btnNewStar_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.pictureBox7);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.textBox_Color_Star);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Controls.Add(this.label13);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.textBox_Radius_Star);
            this.panel8.Controls.Add(this.textBox_Mass_Star);
            this.panel8.Controls.Add(this.textBox_Name_Star);
            this.panel8.Controls.Add(this.textBox_Id_Star);
            this.panel8.Location = new System.Drawing.Point(20, 292);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(666, 224);
            this.panel8.TabIndex = 19;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(312, 15);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(354, 204);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 21;
            this.pictureBox7.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 158);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 16);
            this.label10.TabIndex = 15;
            this.label10.Text = "Колір:";
            // 
            // textBox_Color_Star
            // 
            this.textBox_Color_Star.Location = new System.Drawing.Point(85, 155);
            this.textBox_Color_Star.Name = "textBox_Color_Star";
            this.textBox_Color_Star.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_Star.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "Радіус:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(35, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 16);
            this.label13.TabIndex = 9;
            this.label13.Text = "Маса:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 16);
            this.label14.TabIndex = 8;
            this.label14.Text = "Назва:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(55, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 16);
            this.label19.TabIndex = 3;
            this.label19.Text = "ID:";
            // 
            // textBox_Radius_Star
            // 
            this.textBox_Radius_Star.Location = new System.Drawing.Point(85, 127);
            this.textBox_Radius_Star.Name = "textBox_Radius_Star";
            this.textBox_Radius_Star.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_Star.TabIndex = 6;
            // 
            // textBox_Mass_Star
            // 
            this.textBox_Mass_Star.Location = new System.Drawing.Point(85, 99);
            this.textBox_Mass_Star.Name = "textBox_Mass_Star";
            this.textBox_Mass_Star.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_Star.TabIndex = 5;
            // 
            // textBox_Name_Star
            // 
            this.textBox_Name_Star.Location = new System.Drawing.Point(85, 71);
            this.textBox_Name_Star.Name = "textBox_Name_Star";
            this.textBox_Name_Star.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_Star.TabIndex = 4;
            // 
            // textBox_Id_Star
            // 
            this.textBox_Id_Star.Location = new System.Drawing.Point(85, 43);
            this.textBox_Id_Star.Name = "textBox_Id_Star";
            this.textBox_Id_Star.Size = new System.Drawing.Size(196, 22);
            this.textBox_Id_Star.TabIndex = 3;
            // 
            // dataGridViewStars
            // 
            this.dataGridViewStars.AllowUserToAddRows = false;
            this.dataGridViewStars.AllowUserToDeleteRows = false;
            this.dataGridViewStars.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewStars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStars.Location = new System.Drawing.Point(9, 72);
            this.dataGridViewStars.Name = "dataGridViewStars";
            this.dataGridViewStars.ReadOnly = true;
            this.dataGridViewStars.RowHeadersWidth = 51;
            this.dataGridViewStars.RowTemplate.Height = 24;
            this.dataGridViewStars.Size = new System.Drawing.Size(850, 214);
            this.dataGridViewStars.TabIndex = 18;
            this.dataGridViewStars.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStars_CellClick);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pictureBox8);
            this.panel9.Controls.Add(this.pictureBox9);
            this.panel9.Controls.Add(this.textBox_SearchStars);
            this.panel9.Controls.Add(this.btnRefresh_Stars);
            this.panel9.Controls.Add(this.btnClearStars);
            this.panel9.Location = new System.Drawing.Point(7, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(852, 72);
            this.panel9.TabIndex = 17;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(314, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(71, 68);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 15;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox9.BackgroundImage")));
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(13, -1);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(298, 68);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 14;
            this.pictureBox9.TabStop = false;
            // 
            // textBox_SearchStars
            // 
            this.textBox_SearchStars.Location = new System.Drawing.Point(415, 12);
            this.textBox_SearchStars.Multiline = true;
            this.textBox_SearchStars.Name = "textBox_SearchStars";
            this.textBox_SearchStars.Size = new System.Drawing.Size(238, 37);
            this.textBox_SearchStars.TabIndex = 1;
            this.textBox_SearchStars.TextChanged += new System.EventHandler(this.textBox_SearchStars_TextChanged);
            // 
            // btnRefresh_Stars
            // 
            this.btnRefresh_Stars.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh_Stars.BackgroundImage")));
            this.btnRefresh_Stars.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh_Stars.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh_Stars.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh_Stars.Name = "btnRefresh_Stars";
            this.btnRefresh_Stars.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh_Stars.TabIndex = 0;
            this.btnRefresh_Stars.UseVisualStyleBackColor = true;
            this.btnRefresh_Stars.Click += new System.EventHandler(this.btnRefresh_Stars_Click);
            // 
            // btnClearStars
            // 
            this.btnClearStars.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearStars.BackgroundImage")));
            this.btnClearStars.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClearStars.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearStars.Location = new System.Drawing.Point(768, 3);
            this.btnClearStars.Name = "btnClearStars";
            this.btnClearStars.Size = new System.Drawing.Size(50, 50);
            this.btnClearStars.TabIndex = 0;
            this.btnClearStars.UseVisualStyleBackColor = true;
            this.btnClearStars.Click += new System.EventHandler(this.btnClearStars_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(866, 532);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Планети";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnChange);
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnDelete);
            this.panel4.Controls.Add(this.btnNew);
            this.panel4.Location = new System.Drawing.Point(692, 247);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(167, 271);
            this.panel4.TabIndex = 12;
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.White;
            this.btnChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChange.Location = new System.Drawing.Point(13, 147);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(138, 36);
            this.btnChange.TabIndex = 3;
            this.btnChange.Text = "Змінити";
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.White;
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(13, 193);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 36);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Зберегти";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(13, 101);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(138, 36);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Видалити";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.White;
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.Location = new System.Drawing.Point(13, 53);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(138, 36);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "Новий запис";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.textBox_Satellite_id);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.textBox_Sun_id);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.textBox_Color);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.textBox_Type_planet);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.textBox_Distance_Sun);
            this.panel5.Controls.Add(this.textBox_Radius);
            this.panel5.Controls.Add(this.textBox_Mass);
            this.panel5.Controls.Add(this.textBox_Name);
            this.panel5.Controls.Add(this.textBox_id);
            this.panel5.Location = new System.Drawing.Point(0, 247);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(708, 271);
            this.panel5.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(346, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(340, 222);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 239);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "ID Супутників:";
            // 
            // textBox_Satellite_id
            // 
            this.textBox_Satellite_id.Location = new System.Drawing.Point(144, 239);
            this.textBox_Satellite_id.Name = "textBox_Satellite_id";
            this.textBox_Satellite_id.Size = new System.Drawing.Size(188, 22);
            this.textBox_Satellite_id.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "ID Сонця:";
            // 
            // textBox_Sun_id
            // 
            this.textBox_Sun_id.Location = new System.Drawing.Point(144, 211);
            this.textBox_Sun_id.Name = "textBox_Sun_id";
            this.textBox_Sun_id.Size = new System.Drawing.Size(188, 22);
            this.textBox_Sun_id.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Колір:";
            // 
            // textBox_Color
            // 
            this.textBox_Color.Location = new System.Drawing.Point(144, 183);
            this.textBox_Color.Name = "textBox_Color";
            this.textBox_Color.Size = new System.Drawing.Size(188, 22);
            this.textBox_Color.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "Тип планети:";
            // 
            // textBox_Type_planet
            // 
            this.textBox_Type_planet.Location = new System.Drawing.Point(144, 155);
            this.textBox_Type_planet.Name = "textBox_Type_planet";
            this.textBox_Type_planet.Size = new System.Drawing.Size(188, 22);
            this.textBox_Type_planet.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-3, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Дистанція до сонця:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Радіус:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Маса:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Назва:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID:";
            // 
            // textBox_Distance_Sun
            // 
            this.textBox_Distance_Sun.Location = new System.Drawing.Point(144, 127);
            this.textBox_Distance_Sun.Name = "textBox_Distance_Sun";
            this.textBox_Distance_Sun.Size = new System.Drawing.Size(188, 22);
            this.textBox_Distance_Sun.TabIndex = 7;
            // 
            // textBox_Radius
            // 
            this.textBox_Radius.Location = new System.Drawing.Point(144, 99);
            this.textBox_Radius.Name = "textBox_Radius";
            this.textBox_Radius.Size = new System.Drawing.Size(188, 22);
            this.textBox_Radius.TabIndex = 6;
            // 
            // textBox_Mass
            // 
            this.textBox_Mass.Location = new System.Drawing.Point(144, 71);
            this.textBox_Mass.Name = "textBox_Mass";
            this.textBox_Mass.Size = new System.Drawing.Size(188, 22);
            this.textBox_Mass.TabIndex = 5;
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(144, 43);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(188, 22);
            this.textBox_Name.TabIndex = 4;
            // 
            // textBox_id
            // 
            this.textBox_id.Location = new System.Drawing.Point(144, 15);
            this.textBox_id.Name = "textBox_id";
            this.textBox_id.Size = new System.Drawing.Size(188, 22);
            this.textBox_id.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 74);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(851, 167);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.textBox_search);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 62);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(314, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(73, 63);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 14;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-3, -6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(329, 75);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // textBox_search
            // 
            this.textBox_search.Location = new System.Drawing.Point(415, 12);
            this.textBox_search.Multiline = true;
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(238, 37);
            this.textBox_search.TabIndex = 1;
            this.textBox_search.TextChanged += new System.EventHandler(this.textBox_search_TextChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh.BackgroundImage")));
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClear.BackgroundImage")));
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Location = new System.Drawing.Point(768, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(50, 50);
            this.btnClear.TabIndex = 0;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Controls.Add(this.dataGridViewSatellites);
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(866, 532);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Супутники";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnChangeSatellite);
            this.panel2.Controls.Add(this.btnUpdateSatellites);
            this.panel2.Controls.Add(this.btnDeleteSatellite);
            this.panel2.Controls.Add(this.btnNewSatellite);
            this.panel2.Location = new System.Drawing.Point(692, 294);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(167, 224);
            this.panel2.TabIndex = 16;
            // 
            // btnChangeSatellite
            // 
            this.btnChangeSatellite.BackColor = System.Drawing.Color.White;
            this.btnChangeSatellite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeSatellite.Location = new System.Drawing.Point(17, 123);
            this.btnChangeSatellite.Name = "btnChangeSatellite";
            this.btnChangeSatellite.Size = new System.Drawing.Size(138, 36);
            this.btnChangeSatellite.TabIndex = 3;
            this.btnChangeSatellite.Text = "Змінити";
            this.btnChangeSatellite.UseVisualStyleBackColor = false;
            this.btnChangeSatellite.Click += new System.EventHandler(this.btnChangeSatellite_Click);
            // 
            // btnUpdateSatellites
            // 
            this.btnUpdateSatellites.BackColor = System.Drawing.Color.White;
            this.btnUpdateSatellites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateSatellites.Location = new System.Drawing.Point(17, 169);
            this.btnUpdateSatellites.Name = "btnUpdateSatellites";
            this.btnUpdateSatellites.Size = new System.Drawing.Size(138, 36);
            this.btnUpdateSatellites.TabIndex = 2;
            this.btnUpdateSatellites.Text = "Зберегти";
            this.btnUpdateSatellites.UseVisualStyleBackColor = false;
            this.btnUpdateSatellites.Click += new System.EventHandler(this.btnUpdateSatellites_Click);
            // 
            // btnDeleteSatellite
            // 
            this.btnDeleteSatellite.BackColor = System.Drawing.Color.White;
            this.btnDeleteSatellite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteSatellite.Location = new System.Drawing.Point(17, 77);
            this.btnDeleteSatellite.Name = "btnDeleteSatellite";
            this.btnDeleteSatellite.Size = new System.Drawing.Size(138, 36);
            this.btnDeleteSatellite.TabIndex = 1;
            this.btnDeleteSatellite.Text = "Видалити";
            this.btnDeleteSatellite.UseVisualStyleBackColor = false;
            this.btnDeleteSatellite.Click += new System.EventHandler(this.btnDeleteSatellite_Click);
            // 
            // btnNewSatellite
            // 
            this.btnNewSatellite.BackColor = System.Drawing.Color.White;
            this.btnNewSatellite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewSatellite.Location = new System.Drawing.Point(17, 29);
            this.btnNewSatellite.Name = "btnNewSatellite";
            this.btnNewSatellite.Size = new System.Drawing.Size(138, 36);
            this.btnNewSatellite.TabIndex = 0;
            this.btnNewSatellite.Text = "Новий запис";
            this.btnNewSatellite.UseVisualStyleBackColor = false;
            this.btnNewSatellite.Click += new System.EventHandler(this.btnNewSatellite_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.textBox_Satallite_Color);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.textBox_Satallite_Radius);
            this.panel3.Controls.Add(this.textBox_Satallite_Mass);
            this.panel3.Controls.Add(this.textBox_Satallite_Name);
            this.panel3.Controls.Add(this.textBox_Satallite_id);
            this.panel3.Location = new System.Drawing.Point(20, 294);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(666, 224);
            this.panel3.TabIndex = 15;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(312, 15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(354, 204);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 21;
            this.pictureBox2.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(33, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 16);
            this.label12.TabIndex = 15;
            this.label12.Text = "Колір:";
            // 
            // textBox_Satallite_Color
            // 
            this.textBox_Satallite_Color.Location = new System.Drawing.Point(85, 155);
            this.textBox_Satallite_Color.Name = "textBox_Satallite_Color";
            this.textBox_Satallite_Color.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_Color.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(26, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 16);
            this.label15.TabIndex = 10;
            this.label15.Text = "Радіус:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 16);
            this.label16.TabIndex = 9;
            this.label16.Text = "Маса:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(26, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 16);
            this.label17.TabIndex = 8;
            this.label17.Text = "Назва:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(55, 46);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 16);
            this.label18.TabIndex = 3;
            this.label18.Text = "ID:";
            // 
            // textBox_Satallite_Radius
            // 
            this.textBox_Satallite_Radius.Location = new System.Drawing.Point(85, 127);
            this.textBox_Satallite_Radius.Name = "textBox_Satallite_Radius";
            this.textBox_Satallite_Radius.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_Radius.TabIndex = 6;
            // 
            // textBox_Satallite_Mass
            // 
            this.textBox_Satallite_Mass.Location = new System.Drawing.Point(85, 99);
            this.textBox_Satallite_Mass.Name = "textBox_Satallite_Mass";
            this.textBox_Satallite_Mass.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_Mass.TabIndex = 5;
            // 
            // textBox_Satallite_Name
            // 
            this.textBox_Satallite_Name.Location = new System.Drawing.Point(85, 71);
            this.textBox_Satallite_Name.Name = "textBox_Satallite_Name";
            this.textBox_Satallite_Name.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_Name.TabIndex = 4;
            // 
            // textBox_Satallite_id
            // 
            this.textBox_Satallite_id.Location = new System.Drawing.Point(85, 43);
            this.textBox_Satallite_id.Name = "textBox_Satallite_id";
            this.textBox_Satallite_id.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_id.TabIndex = 3;
            // 
            // dataGridViewSatellites
            // 
            this.dataGridViewSatellites.AllowUserToAddRows = false;
            this.dataGridViewSatellites.AllowUserToDeleteRows = false;
            this.dataGridViewSatellites.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewSatellites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSatellites.Location = new System.Drawing.Point(9, 74);
            this.dataGridViewSatellites.Name = "dataGridViewSatellites";
            this.dataGridViewSatellites.ReadOnly = true;
            this.dataGridViewSatellites.RowHeadersWidth = 51;
            this.dataGridViewSatellites.RowTemplate.Height = 24;
            this.dataGridViewSatellites.Size = new System.Drawing.Size(850, 214);
            this.dataGridViewSatellites.TabIndex = 14;
            this.dataGridViewSatellites.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSatellites_CellClick);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox6);
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Controls.Add(this.textBox_SearchSatellites);
            this.panel6.Controls.Add(this.btnRefresh_Satallites);
            this.panel6.Controls.Add(this.btnClearSatellites);
            this.panel6.Location = new System.Drawing.Point(7, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(852, 62);
            this.panel6.TabIndex = 13;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(313, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(68, 63);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 15;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(-4, -6);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(329, 75);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 14;
            this.pictureBox5.TabStop = false;
            // 
            // textBox_SearchSatellites
            // 
            this.textBox_SearchSatellites.Location = new System.Drawing.Point(415, 12);
            this.textBox_SearchSatellites.Multiline = true;
            this.textBox_SearchSatellites.Name = "textBox_SearchSatellites";
            this.textBox_SearchSatellites.Size = new System.Drawing.Size(238, 37);
            this.textBox_SearchSatellites.TabIndex = 1;
            this.textBox_SearchSatellites.TextChanged += new System.EventHandler(this.textBox_SearchSatellites_TextChanged);
            // 
            // btnRefresh_Satallites
            // 
            this.btnRefresh_Satallites.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh_Satallites.BackgroundImage")));
            this.btnRefresh_Satallites.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh_Satallites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh_Satallites.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh_Satallites.Name = "btnRefresh_Satallites";
            this.btnRefresh_Satallites.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh_Satallites.TabIndex = 0;
            this.btnRefresh_Satallites.UseVisualStyleBackColor = true;
            this.btnRefresh_Satallites.Click += new System.EventHandler(this.btnRefresh_Satallites_Click);
            // 
            // btnClearSatellites
            // 
            this.btnClearSatellites.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearSatellites.BackgroundImage")));
            this.btnClearSatellites.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClearSatellites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearSatellites.Location = new System.Drawing.Point(768, 3);
            this.btnClearSatellites.Name = "btnClearSatellites";
            this.btnClearSatellites.Size = new System.Drawing.Size(50, 50);
            this.btnClearSatellites.TabIndex = 0;
            this.btnClearSatellites.UseVisualStyleBackColor = true;
            this.btnClearSatellites.Click += new System.EventHandler(this.btnClearSatellites_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel13);
            this.tabPage5.Controls.Add(this.panel14);
            this.tabPage5.Controls.Add(this.dataGridViewMeteorites);
            this.tabPage5.Controls.Add(this.panel15);
            this.tabPage5.Location = new System.Drawing.Point(4, 28);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(866, 532);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Метеорити";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label26);
            this.panel13.Controls.Add(this.textBox_IDPROSTIR_Meteorite);
            this.panel13.Controls.Add(this.pictureBox13);
            this.panel13.Controls.Add(this.label27);
            this.panel13.Controls.Add(this.textBox_Color_Meteorite);
            this.panel13.Controls.Add(this.label28);
            this.panel13.Controls.Add(this.label29);
            this.panel13.Controls.Add(this.label30);
            this.panel13.Controls.Add(this.label31);
            this.panel13.Controls.Add(this.textBox_Radius_Meteorite);
            this.panel13.Controls.Add(this.textBox_Mass_Meteorite);
            this.panel13.Controls.Add(this.textBox_Name_Meteorite);
            this.panel13.Controls.Add(this.textBox_Id_Meteorite);
            this.panel13.Location = new System.Drawing.Point(20, 292);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(666, 224);
            this.panel13.TabIndex = 23;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 189);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 16);
            this.label26.TabIndex = 23;
            this.label26.Text = "ID простір:";
            // 
            // textBox_IDPROSTIR_Meteorite
            // 
            this.textBox_IDPROSTIR_Meteorite.Location = new System.Drawing.Point(85, 183);
            this.textBox_IDPROSTIR_Meteorite.Name = "textBox_IDPROSTIR_Meteorite";
            this.textBox_IDPROSTIR_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_IDPROSTIR_Meteorite.TabIndex = 22;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox13.BackgroundImage")));
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox13.Location = new System.Drawing.Point(312, 15);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(354, 204);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 21;
            this.pictureBox13.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(33, 158);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 16);
            this.label27.TabIndex = 15;
            this.label27.Text = "Колір:";
            // 
            // textBox_Color_Meteorite
            // 
            this.textBox_Color_Meteorite.Location = new System.Drawing.Point(85, 155);
            this.textBox_Color_Meteorite.Name = "textBox_Color_Meteorite";
            this.textBox_Color_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_Meteorite.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(26, 133);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 16);
            this.label28.TabIndex = 10;
            this.label28.Text = "Радіус:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(35, 102);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 16);
            this.label29.TabIndex = 9;
            this.label29.Text = "Маса:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(26, 74);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 16);
            this.label30.TabIndex = 8;
            this.label30.Text = "Назва:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(55, 46);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(23, 16);
            this.label31.TabIndex = 3;
            this.label31.Text = "ID:";
            // 
            // textBox_Radius_Meteorite
            // 
            this.textBox_Radius_Meteorite.Location = new System.Drawing.Point(85, 127);
            this.textBox_Radius_Meteorite.Name = "textBox_Radius_Meteorite";
            this.textBox_Radius_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_Meteorite.TabIndex = 6;
            // 
            // textBox_Mass_Meteorite
            // 
            this.textBox_Mass_Meteorite.Location = new System.Drawing.Point(85, 99);
            this.textBox_Mass_Meteorite.Name = "textBox_Mass_Meteorite";
            this.textBox_Mass_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_Meteorite.TabIndex = 5;
            // 
            // textBox_Name_Meteorite
            // 
            this.textBox_Name_Meteorite.Location = new System.Drawing.Point(85, 71);
            this.textBox_Name_Meteorite.Name = "textBox_Name_Meteorite";
            this.textBox_Name_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_Meteorite.TabIndex = 4;
            // 
            // textBox_Id_Meteorite
            // 
            this.textBox_Id_Meteorite.Location = new System.Drawing.Point(85, 43);
            this.textBox_Id_Meteorite.Name = "textBox_Id_Meteorite";
            this.textBox_Id_Meteorite.Size = new System.Drawing.Size(196, 22);
            this.textBox_Id_Meteorite.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnChangeMeteorite);
            this.panel14.Controls.Add(this.btnUpdateMeteorite);
            this.panel14.Controls.Add(this.btnDeleteMeteorite);
            this.panel14.Controls.Add(this.btnNewMeteorite);
            this.panel14.Location = new System.Drawing.Point(692, 292);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(167, 224);
            this.panel14.TabIndex = 24;
            // 
            // btnChangeMeteorite
            // 
            this.btnChangeMeteorite.BackColor = System.Drawing.Color.White;
            this.btnChangeMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeMeteorite.Location = new System.Drawing.Point(17, 123);
            this.btnChangeMeteorite.Name = "btnChangeMeteorite";
            this.btnChangeMeteorite.Size = new System.Drawing.Size(138, 36);
            this.btnChangeMeteorite.TabIndex = 3;
            this.btnChangeMeteorite.Text = "Змінити";
            this.btnChangeMeteorite.UseVisualStyleBackColor = false;
            this.btnChangeMeteorite.Click += new System.EventHandler(this.btnChangeMeteorite_Click);
            // 
            // btnUpdateMeteorite
            // 
            this.btnUpdateMeteorite.BackColor = System.Drawing.Color.White;
            this.btnUpdateMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateMeteorite.Location = new System.Drawing.Point(17, 169);
            this.btnUpdateMeteorite.Name = "btnUpdateMeteorite";
            this.btnUpdateMeteorite.Size = new System.Drawing.Size(138, 36);
            this.btnUpdateMeteorite.TabIndex = 2;
            this.btnUpdateMeteorite.Text = "Зберегти";
            this.btnUpdateMeteorite.UseVisualStyleBackColor = false;
            this.btnUpdateMeteorite.Click += new System.EventHandler(this.btnUpdateMeteorite_Click);
            // 
            // btnDeleteMeteorite
            // 
            this.btnDeleteMeteorite.BackColor = System.Drawing.Color.White;
            this.btnDeleteMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteMeteorite.Location = new System.Drawing.Point(17, 77);
            this.btnDeleteMeteorite.Name = "btnDeleteMeteorite";
            this.btnDeleteMeteorite.Size = new System.Drawing.Size(138, 36);
            this.btnDeleteMeteorite.TabIndex = 1;
            this.btnDeleteMeteorite.Text = "Видалити";
            this.btnDeleteMeteorite.UseVisualStyleBackColor = false;
            this.btnDeleteMeteorite.Click += new System.EventHandler(this.btnDeleteMeteorite_Click);
            // 
            // btnNewMeteorite
            // 
            this.btnNewMeteorite.BackColor = System.Drawing.Color.White;
            this.btnNewMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewMeteorite.Location = new System.Drawing.Point(17, 29);
            this.btnNewMeteorite.Name = "btnNewMeteorite";
            this.btnNewMeteorite.Size = new System.Drawing.Size(138, 36);
            this.btnNewMeteorite.TabIndex = 0;
            this.btnNewMeteorite.Text = "Новий запис";
            this.btnNewMeteorite.UseVisualStyleBackColor = false;
            this.btnNewMeteorite.Click += new System.EventHandler(this.btnNewMeteorite_Click);
            // 
            // dataGridViewMeteorites
            // 
            this.dataGridViewMeteorites.AllowUserToAddRows = false;
            this.dataGridViewMeteorites.AllowUserToDeleteRows = false;
            this.dataGridViewMeteorites.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewMeteorites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMeteorites.Location = new System.Drawing.Point(9, 72);
            this.dataGridViewMeteorites.Name = "dataGridViewMeteorites";
            this.dataGridViewMeteorites.ReadOnly = true;
            this.dataGridViewMeteorites.RowHeadersWidth = 51;
            this.dataGridViewMeteorites.RowTemplate.Height = 24;
            this.dataGridViewMeteorites.Size = new System.Drawing.Size(850, 214);
            this.dataGridViewMeteorites.TabIndex = 22;
            this.dataGridViewMeteorites.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMeteorites_CellClick);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.pictureBox14);
            this.panel15.Controls.Add(this.pictureBox15);
            this.panel15.Controls.Add(this.textBox_SearchMeteorites);
            this.panel15.Controls.Add(this.btnRefresh_Meteorites);
            this.panel15.Controls.Add(this.btnClearMeteorite);
            this.panel15.Location = new System.Drawing.Point(7, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(852, 62);
            this.panel15.TabIndex = 21;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox14.BackgroundImage")));
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox14.Location = new System.Drawing.Point(279, 12);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(55, 47);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 15;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox15.BackgroundImage")));
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox15.Location = new System.Drawing.Point(3, -5);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(291, 75);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 14;
            this.pictureBox15.TabStop = false;
            // 
            // textBox_SearchMeteorites
            // 
            this.textBox_SearchMeteorites.Location = new System.Drawing.Point(415, 12);
            this.textBox_SearchMeteorites.Multiline = true;
            this.textBox_SearchMeteorites.Name = "textBox_SearchMeteorites";
            this.textBox_SearchMeteorites.Size = new System.Drawing.Size(238, 37);
            this.textBox_SearchMeteorites.TabIndex = 1;
            this.textBox_SearchMeteorites.TextChanged += new System.EventHandler(this.textBox_SearchMeteorites_TextChanged);
            // 
            // btnRefresh_Meteorites
            // 
            this.btnRefresh_Meteorites.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh_Meteorites.BackgroundImage")));
            this.btnRefresh_Meteorites.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh_Meteorites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh_Meteorites.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh_Meteorites.Name = "btnRefresh_Meteorites";
            this.btnRefresh_Meteorites.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh_Meteorites.TabIndex = 0;
            this.btnRefresh_Meteorites.UseVisualStyleBackColor = true;
            this.btnRefresh_Meteorites.Click += new System.EventHandler(this.btnRefresh_Meteorites_Click);
            // 
            // btnClearMeteorite
            // 
            this.btnClearMeteorite.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearMeteorite.BackgroundImage")));
            this.btnClearMeteorite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClearMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearMeteorite.Location = new System.Drawing.Point(768, 3);
            this.btnClearMeteorite.Name = "btnClearMeteorite";
            this.btnClearMeteorite.Size = new System.Drawing.Size(50, 50);
            this.btnClearMeteorite.TabIndex = 0;
            this.btnClearMeteorite.UseVisualStyleBackColor = true;
            this.btnClearMeteorite.Click += new System.EventHandler(this.btnClearMeteorite_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel10);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Controls.Add(this.panel12);
            this.tabPage4.Controls.Add(this.dataGridViewComets);
            this.tabPage4.Location = new System.Drawing.Point(4, 28);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(866, 532);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Комети";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label25);
            this.panel10.Controls.Add(this.textBox_IDPROSTIR_Comets);
            this.panel10.Controls.Add(this.pictureBox10);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Controls.Add(this.textBox_Color_Comets);
            this.panel10.Controls.Add(this.label21);
            this.panel10.Controls.Add(this.label22);
            this.panel10.Controls.Add(this.label24);
            this.panel10.Controls.Add(this.label23);
            this.panel10.Controls.Add(this.textBox_Radius_Comets);
            this.panel10.Controls.Add(this.textBox_Mass_Comets);
            this.panel10.Controls.Add(this.textBox_Name_Comets);
            this.panel10.Controls.Add(this.textBox_Id_Comets);
            this.panel10.Location = new System.Drawing.Point(20, 292);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(666, 224);
            this.panel10.TabIndex = 19;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 189);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 16);
            this.label25.TabIndex = 23;
            this.label25.Text = "ID простір:";
            // 
            // textBox_IDPROSTIR_Comets
            // 
            this.textBox_IDPROSTIR_Comets.Location = new System.Drawing.Point(85, 183);
            this.textBox_IDPROSTIR_Comets.Name = "textBox_IDPROSTIR_Comets";
            this.textBox_IDPROSTIR_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_IDPROSTIR_Comets.TabIndex = 22;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.Location = new System.Drawing.Point(312, 15);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(354, 204);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 21;
            this.pictureBox10.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(33, 158);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 16);
            this.label20.TabIndex = 15;
            this.label20.Text = "Колір:";
            // 
            // textBox_Color_Comets
            // 
            this.textBox_Color_Comets.Location = new System.Drawing.Point(85, 155);
            this.textBox_Color_Comets.Name = "textBox_Color_Comets";
            this.textBox_Color_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_Comets.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(26, 133);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 16);
            this.label21.TabIndex = 10;
            this.label21.Text = "Радіус:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(35, 102);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 16);
            this.label22.TabIndex = 9;
            this.label22.Text = "Маса:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(26, 74);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 16);
            this.label24.TabIndex = 8;
            this.label24.Text = "Назва:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(55, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(23, 16);
            this.label23.TabIndex = 3;
            this.label23.Text = "ID:";
            // 
            // textBox_Radius_Comets
            // 
            this.textBox_Radius_Comets.Location = new System.Drawing.Point(85, 127);
            this.textBox_Radius_Comets.Name = "textBox_Radius_Comets";
            this.textBox_Radius_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_Comets.TabIndex = 6;
            // 
            // textBox_Mass_Comets
            // 
            this.textBox_Mass_Comets.Location = new System.Drawing.Point(85, 99);
            this.textBox_Mass_Comets.Name = "textBox_Mass_Comets";
            this.textBox_Mass_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_Comets.TabIndex = 5;
            // 
            // textBox_Name_Comets
            // 
            this.textBox_Name_Comets.Location = new System.Drawing.Point(85, 71);
            this.textBox_Name_Comets.Name = "textBox_Name_Comets";
            this.textBox_Name_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_Comets.TabIndex = 4;
            // 
            // textBox_Id_Comets
            // 
            this.textBox_Id_Comets.Location = new System.Drawing.Point(85, 43);
            this.textBox_Id_Comets.Name = "textBox_Id_Comets";
            this.textBox_Id_Comets.Size = new System.Drawing.Size(196, 22);
            this.textBox_Id_Comets.TabIndex = 3;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnChangeComets);
            this.panel11.Controls.Add(this.btnUpdateComets);
            this.panel11.Controls.Add(this.btnDeleteComets);
            this.panel11.Controls.Add(this.btnNewComets);
            this.panel11.Location = new System.Drawing.Point(692, 292);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(167, 224);
            this.panel11.TabIndex = 20;
            // 
            // btnChangeComets
            // 
            this.btnChangeComets.BackColor = System.Drawing.Color.White;
            this.btnChangeComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangeComets.Location = new System.Drawing.Point(17, 123);
            this.btnChangeComets.Name = "btnChangeComets";
            this.btnChangeComets.Size = new System.Drawing.Size(138, 36);
            this.btnChangeComets.TabIndex = 3;
            this.btnChangeComets.Text = "Змінити";
            this.btnChangeComets.UseVisualStyleBackColor = false;
            this.btnChangeComets.Click += new System.EventHandler(this.btnChangeComets_Click);
            // 
            // btnUpdateComets
            // 
            this.btnUpdateComets.BackColor = System.Drawing.Color.White;
            this.btnUpdateComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateComets.Location = new System.Drawing.Point(17, 169);
            this.btnUpdateComets.Name = "btnUpdateComets";
            this.btnUpdateComets.Size = new System.Drawing.Size(138, 36);
            this.btnUpdateComets.TabIndex = 2;
            this.btnUpdateComets.Text = "Зберегти";
            this.btnUpdateComets.UseVisualStyleBackColor = false;
            this.btnUpdateComets.Click += new System.EventHandler(this.btnUpdateComets_Click);
            // 
            // btnDeleteComets
            // 
            this.btnDeleteComets.BackColor = System.Drawing.Color.White;
            this.btnDeleteComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteComets.Location = new System.Drawing.Point(17, 77);
            this.btnDeleteComets.Name = "btnDeleteComets";
            this.btnDeleteComets.Size = new System.Drawing.Size(138, 36);
            this.btnDeleteComets.TabIndex = 1;
            this.btnDeleteComets.Text = "Видалити";
            this.btnDeleteComets.UseVisualStyleBackColor = false;
            this.btnDeleteComets.Click += new System.EventHandler(this.btnDeleteComets_Click);
            // 
            // btnNewComets
            // 
            this.btnNewComets.BackColor = System.Drawing.Color.White;
            this.btnNewComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNewComets.Location = new System.Drawing.Point(17, 29);
            this.btnNewComets.Name = "btnNewComets";
            this.btnNewComets.Size = new System.Drawing.Size(138, 36);
            this.btnNewComets.TabIndex = 0;
            this.btnNewComets.Text = "Новий запис";
            this.btnNewComets.UseVisualStyleBackColor = false;
            this.btnNewComets.Click += new System.EventHandler(this.btnNewComets_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.pictureBox11);
            this.panel12.Controls.Add(this.pictureBox12);
            this.panel12.Controls.Add(this.textBox_SearchComets);
            this.panel12.Controls.Add(this.btnRefresh_Comets);
            this.panel12.Controls.Add(this.btnClearComets);
            this.panel12.Location = new System.Drawing.Point(7, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(852, 62);
            this.panel12.TabIndex = 17;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.Location = new System.Drawing.Point(277, 0);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(111, 63);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 15;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox12.BackgroundImage")));
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox12.Location = new System.Drawing.Point(24, -5);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(247, 75);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 14;
            this.pictureBox12.TabStop = false;
            // 
            // textBox_SearchComets
            // 
            this.textBox_SearchComets.Location = new System.Drawing.Point(415, 12);
            this.textBox_SearchComets.Multiline = true;
            this.textBox_SearchComets.Name = "textBox_SearchComets";
            this.textBox_SearchComets.Size = new System.Drawing.Size(238, 37);
            this.textBox_SearchComets.TabIndex = 1;
            this.textBox_SearchComets.TextChanged += new System.EventHandler(this.textBox_SearchComets_TextChanged);
            // 
            // btnRefresh_Comets
            // 
            this.btnRefresh_Comets.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh_Comets.BackgroundImage")));
            this.btnRefresh_Comets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh_Comets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh_Comets.Location = new System.Drawing.Point(693, 3);
            this.btnRefresh_Comets.Name = "btnRefresh_Comets";
            this.btnRefresh_Comets.Size = new System.Drawing.Size(50, 50);
            this.btnRefresh_Comets.TabIndex = 0;
            this.btnRefresh_Comets.UseVisualStyleBackColor = true;
            this.btnRefresh_Comets.Click += new System.EventHandler(this.btnRefresh_Comets_Click);
            // 
            // btnClearComets
            // 
            this.btnClearComets.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearComets.BackgroundImage")));
            this.btnClearComets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClearComets.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearComets.Location = new System.Drawing.Point(768, 3);
            this.btnClearComets.Name = "btnClearComets";
            this.btnClearComets.Size = new System.Drawing.Size(50, 50);
            this.btnClearComets.TabIndex = 0;
            this.btnClearComets.UseVisualStyleBackColor = true;
            this.btnClearComets.Click += new System.EventHandler(this.btnClearComets_Click);
            // 
            // dataGridViewComets
            // 
            this.dataGridViewComets.AllowUserToAddRows = false;
            this.dataGridViewComets.AllowUserToDeleteRows = false;
            this.dataGridViewComets.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridViewComets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComets.Location = new System.Drawing.Point(9, 72);
            this.dataGridViewComets.Name = "dataGridViewComets";
            this.dataGridViewComets.ReadOnly = true;
            this.dataGridViewComets.RowHeadersWidth = 51;
            this.dataGridViewComets.RowTemplate.Height = 24;
            this.dataGridViewComets.Size = new System.Drawing.Size(850, 214);
            this.dataGridViewComets.TabIndex = 18;
            this.dataGridViewComets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewComets_CellClick);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.CreateBackup);
            this.tabPage7.Controls.Add(this.pictureBox19);
            this.tabPage7.Location = new System.Drawing.Point(4, 28);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(866, 532);
            this.tabPage7.TabIndex = 5;
            this.tabPage7.Text = "BACKUP OUTER SPACE DATABASE";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // CreateBackup
            // 
            this.CreateBackup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreateBackup.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CreateBackup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CreateBackup.Location = new System.Drawing.Point(341, 366);
            this.CreateBackup.Name = "CreateBackup";
            this.CreateBackup.Size = new System.Drawing.Size(196, 48);
            this.CreateBackup.TabIndex = 55;
            this.CreateBackup.Text = "Сторити BACKUP";
            this.CreateBackup.UseVisualStyleBackColor = false;
            this.CreateBackup.Click += new System.EventHandler(this.CreateBackup_Click);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox19.BackgroundImage")));
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox19.Location = new System.Drawing.Point(178, 41);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(519, 289);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 22;
            this.pictureBox19.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.керуванняToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(2, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(106, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // керуванняToolStripMenuItem
            // 
            this.керуванняToolStripMenuItem.Name = "керуванняToolStripMenuItem";
            this.керуванняToolStripMenuItem.Size = new System.Drawing.Size(98, 24);
            this.керуванняToolStripMenuItem.Text = "Керування";
            this.керуванняToolStripMenuItem.Click += new System.EventHandler(this.керуванняToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripUser});
            this.toolStrip1.Location = new System.Drawing.Point(718, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(115, 27);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripUser
            // 
            this.toolStripUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripUser.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripUser.Name = "toolStripUser";
            this.toolStripUser.Size = new System.Drawing.Size(100, 27);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 600);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Outer space database";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TabControl.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOuterSpace)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStars)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSatellites)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMeteorites)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComets)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox_Distance_Sun;
        private System.Windows.Forms.TextBox textBox_Radius;
        private System.Windows.Forms.TextBox textBox_Mass;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.TextBox textBox_id;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox_Type_planet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Color;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_Sun_id;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_Satellite_id;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnChangeSatellite;
        private System.Windows.Forms.Button btnUpdateSatellites;
        private System.Windows.Forms.Button btnDeleteSatellite;
        private System.Windows.Forms.Button btnNewSatellite;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_Satallite_Color;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox_Satallite_Radius;
        private System.Windows.Forms.TextBox textBox_Satallite_Mass;
        private System.Windows.Forms.TextBox textBox_Satallite_Name;
        private System.Windows.Forms.TextBox textBox_Satallite_id;
        private System.Windows.Forms.DataGridView dataGridViewSatellites;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBox_SearchSatellites;
        private System.Windows.Forms.Button btnRefresh_Satallites;
        private System.Windows.Forms.Button btnClearSatellites;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnChangeStar;
        private System.Windows.Forms.Button btnUpdateStar;
        private System.Windows.Forms.Button btnDeleteStar;
        private System.Windows.Forms.Button btnNewStar;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_Color_Star;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox_Radius_Star;
        private System.Windows.Forms.TextBox textBox_Mass_Star;
        private System.Windows.Forms.TextBox textBox_Name_Star;
        private System.Windows.Forms.TextBox textBox_Id_Star;
        private System.Windows.Forms.DataGridView dataGridViewStars;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TextBox textBox_SearchStars;
        private System.Windows.Forms.Button btnRefresh_Stars;
        private System.Windows.Forms.Button btnClearStars;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox_Color_Comets;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox_Radius_Comets;
        private System.Windows.Forms.TextBox textBox_Mass_Comets;
        private System.Windows.Forms.TextBox textBox_Name_Comets;
        private System.Windows.Forms.TextBox textBox_Id_Comets;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnChangeComets;
        private System.Windows.Forms.Button btnUpdateComets;
        private System.Windows.Forms.Button btnDeleteComets;
        private System.Windows.Forms.Button btnNewComets;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.TextBox textBox_SearchComets;
        private System.Windows.Forms.Button btnRefresh_Comets;
        private System.Windows.Forms.Button btnClearComets;
        private System.Windows.Forms.DataGridView dataGridViewComets;
        private System.Windows.Forms.TextBox textBox_IDPROSTIR_Comets;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox_IDPROSTIR_Meteorite;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox_Color_Meteorite;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox_Radius_Meteorite;
        private System.Windows.Forms.TextBox textBox_Mass_Meteorite;
        private System.Windows.Forms.TextBox textBox_Name_Meteorite;
        private System.Windows.Forms.TextBox textBox_Id_Meteorite;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnChangeMeteorite;
        private System.Windows.Forms.Button btnUpdateMeteorite;
        private System.Windows.Forms.Button btnDeleteMeteorite;
        private System.Windows.Forms.Button btnNewMeteorite;
        private System.Windows.Forms.DataGridView dataGridViewMeteorites;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.TextBox textBox_SearchMeteorites;
        private System.Windows.Forms.Button btnRefresh_Meteorites;
        private System.Windows.Forms.Button btnClearMeteorite;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox_IDPROSTIR_OuterSpace;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox_Color_OuterSpace;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox_Radius_OuterSpace;
        private System.Windows.Forms.TextBox textBox_Mass_OuterSpace;
        private System.Windows.Forms.TextBox textBox_Name_OuterSpace;
        private System.Windows.Forms.TextBox textBox_SunnySpace_id_OuterSpace;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button btnChangeOuterSpace;
        private System.Windows.Forms.Button btnUpdateOuterSpace;
        private System.Windows.Forms.Button btnDeleteOuterSpace;
        private System.Windows.Forms.Button btnNewOuterSpace;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.TextBox textBox_SearchOuterSpace;
        private System.Windows.Forms.Button btnRefresh_OuterSpace;
        private System.Windows.Forms.Button btnClearOuterSpace;
        private System.Windows.Forms.DataGridView dataGridViewOuterSpace;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Button CreateBackup;
        private System.Windows.Forms.ToolStripMenuItem керуванняToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripTextBox toolStripUser;
    }
}

