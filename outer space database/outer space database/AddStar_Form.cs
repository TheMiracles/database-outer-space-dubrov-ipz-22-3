﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace outer_space_database
{
    public partial class AddStar_Form : Form
    {

        DataBase dataBase = new DataBase();

        public AddStar_Form()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnAddStar_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Sun_Id;
            var Name = textBox_StarName.Text;
            int Mass;
            int Radius;
            var Color = textBox_StarColor.Text;

            if (int.TryParse(textBox_Star_Id2.Text, out Sun_Id) &&
                int.TryParse(textBox_StarMass.Text, out Mass) &&
                int.TryParse(textBox_StarRadius.Text, out Radius))
            {
                var addQuery = $"INSERT INTO Sun (Sun_id, Name, Mass, Radius, Color) VALUES ('{Sun_Id}', '{Name}', '{Mass}', '{Radius}', '{Color}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Дані про нову зірку успішно додано!", "Успішно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();
        }
    }
}
