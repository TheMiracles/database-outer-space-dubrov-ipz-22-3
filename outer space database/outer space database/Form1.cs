﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.IO;

namespace outer_space_database
{
    enum RoWState
    {
        Existed,
        New,
        Modified,
        ModifiedNew,
        Deleted
    }
    public partial class Form1 : Form
    {
        private readonly checkUser _user;

        DataBase dataBase = new DataBase();

        int selectedRow;


        public Form1(checkUser user)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
            _user = user;
        }

        private void IsAdmin()
        {
            керуванняToolStripMenuItem.Enabled = _user.IsAdmin;
            toolStripUser.Enabled = _user.IsAdmin;
            btnNewOuterSpace.Enabled = _user.IsAdmin;
            btnDeleteOuterSpace.Enabled = _user.IsAdmin;
            btnChangeOuterSpace.Enabled = _user.IsAdmin;
            btnUpdateOuterSpace.Enabled = _user.IsAdmin;
            btnNewStar.Enabled = _user.IsAdmin;
            btnDeleteStar.Enabled = _user.IsAdmin;
            btnChangeStar.Enabled = _user.IsAdmin;
            btnUpdateStar.Enabled = _user.IsAdmin;
            btnNew.Enabled = _user.IsAdmin;
            btnDelete.Enabled = _user.IsAdmin;
            btnChange.Enabled = _user.IsAdmin;
            btnUpdate.Enabled = _user.IsAdmin;
            btnNewSatellite.Enabled = _user.IsAdmin;
            btnDeleteSatellite.Enabled = _user.IsAdmin;
            btnChangeSatellite.Enabled = _user.IsAdmin;
            btnUpdateSatellites.Enabled = _user.IsAdmin;
            btnNewMeteorite.Enabled = _user.IsAdmin;
            btnDeleteMeteorite.Enabled = _user.IsAdmin;
            btnChangeMeteorite.Enabled = _user.IsAdmin;
            btnUpdateMeteorite.Enabled = _user.IsAdmin;
            btnNewComets.Enabled = _user.IsAdmin;
            btnDeleteComets.Enabled = _user.IsAdmin;
            btnChangeComets.Enabled = _user.IsAdmin;
            btnUpdateComets.Enabled = _user.IsAdmin;
            CreateBackup.Enabled = _user.IsAdmin;
        }

        private void CreatePlanetColumns()
        {
            dataGridView1.Columns.Add("Planet_id", "ID планети");
            dataGridView1.Columns.Add("Name", "Назва");
            dataGridView1.Columns.Add("Mass", "Маса");
            dataGridView1.Columns.Add("Radius", "Радіус");
            dataGridView1.Columns.Add("Distance_Sun", "Дистанція до сонця");
            dataGridView1.Columns.Add("Type_planet", "Тип планети");
            dataGridView1.Columns.Add("Сolor", "Колір");
            dataGridView1.Columns.Add("Sun_id", "ID Сонця");
            dataGridView1.Columns.Add("Satallite_id", "ID Супутників");
            dataGridView1.Columns.Add("IsNew", String.Empty);
           
        }
        private void CreateSatelliteColumns()
        {
       
            dataGridViewSatellites.Columns.Add("Satallite_id", "ID супутника");
            dataGridViewSatellites.Columns.Add("Name", "Назва супутника");
            dataGridViewSatellites.Columns.Add("Mass", "Маса супутника");
            dataGridViewSatellites.Columns.Add("Radius", "Радіус супутника");
            dataGridViewSatellites.Columns.Add("Color", "Колір супутника");
            dataGridViewSatellites.Columns.Add("IsNew", String.Empty);
        }
        private void CreateStarColumns()
        {
           
            dataGridViewStars.Columns.Add("Sun_id", "ID зірки");
            dataGridViewStars.Columns.Add("Name", "Назва зірки");
            dataGridViewStars.Columns.Add("Mass", "Маса зірки");
            dataGridViewStars.Columns.Add("Radius", "Радіус зірки");
            dataGridViewStars.Columns.Add("Color", "Колір зірки");
            dataGridViewStars.Columns.Add("IsNew", String.Empty);
        }
        private void CreateMeteoriteColumns()
        {
            
            dataGridViewMeteorites.Columns.Add("Meteorites_id", "ID метеориту");
            dataGridViewMeteorites.Columns.Add("Name", "Назва метеориту");
            dataGridViewMeteorites.Columns.Add("Mass", "Маса метеориту");
            dataGridViewMeteorites.Columns.Add("Radius", "Радіус метеориту");
            dataGridViewMeteorites.Columns.Add("Color", "Колір метеориту");
            dataGridViewMeteorites.Columns.Add("Sunnyspace_id", "ID сонячного простору");
            dataGridViewMeteorites.Columns.Add("IsNew", String.Empty);
        }
        private void CreateCometColumns()
        {
           
            dataGridViewComets.Columns.Add("Comets_id", "ID комети");
            dataGridViewComets.Columns.Add("Name", "Назва комети");
            dataGridViewComets.Columns.Add("Mass", "Маса комети");
            dataGridViewComets.Columns.Add("Radius", "Радіус комети");
            dataGridViewComets.Columns.Add("Color", "Колір комети");
            dataGridViewComets.Columns.Add("Sunnyspace_id", "ID сонячного простору");
            dataGridViewComets.Columns.Add("IsNew", String.Empty);
        }
        private void CreateOuterSpaceColumns()
        {
            
            dataGridViewOuterSpace.Columns.Add("SunnySpace_id", "ID космічного простору");
            dataGridViewOuterSpace.Columns.Add("Name", "Назва");
            dataGridViewOuterSpace.Columns.Add("Mass", "Маса");
            dataGridViewOuterSpace.Columns.Add("Radius", "Радіус");
            dataGridViewOuterSpace.Columns.Add("Color", "Колір");
            dataGridViewOuterSpace.Columns.Add("Sun_id", "ID сонячного простору");
            dataGridViewOuterSpace.Columns.Add("IsNew", String.Empty);
        }



        private void ClearPlanetsFields()
        {
            textBox_id.Text = "";
            textBox_Name.Text = "";
            textBox_Mass.Text = "";
            textBox_Radius.Text = "";
            textBox_Distance_Sun.Text = "";
            textBox_Type_planet.Text = "";
            textBox_Color.Text = "";
            textBox_Sun_id.Text = "";
            textBox_Satellite_id.Text = "";
        }
        private void ClearSatelliteFields()
        {
           
            textBox_Satallite_id.Text = "";
            textBox_Satallite_Name.Text = "";
            textBox_Satallite_Mass.Text = "";
            textBox_Satallite_Radius.Text = "";
            textBox_Satallite_Color.Text = "";
            
        }
        private void ClearStarFields()
        {
           
            textBox_Id_Star.Text = "";
            textBox_Name_Star.Text = "";
            textBox_Mass_Star.Text = "";
            textBox_Radius_Star.Text = "";
            textBox_Color_Star.Text = "";
          
        }
        private void ClearMeteoriteFields()
        {
            
            textBox_Id_Meteorite.Text = "";
            textBox_Name_Meteorite.Text = "";
            textBox_Mass_Meteorite.Text = "";
            textBox_Radius_Meteorite.Text = "";
            textBox_Color_Meteorite.Text = "";
            textBox_IDPROSTIR_Meteorite.Text = "";
            
        }
        private void ClearCometFields()
        {
   
            textBox_Id_Comets.Text = "";
            textBox_Name_Comets.Text = "";
            textBox_Mass_Comets.Text = "";
            textBox_Radius_Comets.Text = "";
            textBox_Color_Comets.Text = "";
            textBox_IDPROSTIR_Comets.Text = "";
            
        }
        private void ClearOuterSpaceFields()
        {
            
            textBox_SunnySpace_id_OuterSpace.Text = "";
            textBox_Name_OuterSpace.Text = "";
            textBox_Mass_OuterSpace.Text = "";
            textBox_Radius_OuterSpace.Text = "";
            textBox_Color_OuterSpace.Text = "";
            textBox_IDPROSTIR_OuterSpace.Text = "";
         
        }



        private void ReadSingleRow(DataGridView dgw, IDataRecord record)
        {
            dgw.Rows.Add(
                record.GetInt32(0),//Planet_id
                record.GetString(1),//Name
                record.GetInt64(2),//Mass
                record.GetInt64(3),//Radius
                record.GetInt64(4),//Distance_sun
                record.GetString(5),//Type_Planet
                record.GetString(6),//Color
                record.GetInt32(7),//Sun_id
                record.GetInt32(8),//Satallite_id
                RoWState.ModifiedNew);
        }

        private void ReadSatelliteRow(DataGridView dgw, IDataRecord record)
        {
            dgw.Rows.Add(
                record.GetInt32(0),  // Satallite_id
                record.GetString(1), // Name
                record.GetInt64(2),  // Mass
                record.GetInt64(3),  // Radius
                record.GetString(4), // Color
                RoWState.ModifiedNew // State
            );
        }
        private void ReadStarRow(DataGridView dgw, IDataRecord record)
        {
            dgw.Rows.Add(
                record.GetInt32(0),   // Sun_id
                record.GetString(1), // Name
                record.GetInt64(2),   // Mass
                record.GetInt64(3),   // Radius
                record.GetString(4), // Color
                RoWState.ModifiedNew // State
            );
        }
        private void ReadMeteoriteRow(DataGridView dgw, IDataRecord record)
        {
            dgw.Rows.Add(
            record.GetInt32(0), // Sun_id
            record.GetString(1), // Name
            record.GetValue(2), // Mass
            record.GetValue(3), // Radius
            record.GetString(4), // Color
            record.GetInt32(5), // Sunnyspace_id
            RoWState.ModifiedNew // State

         );
        }
        private void ReadCometRow(DataGridView dgw, IDataRecord record)
        {
            int cometsId = record.IsDBNull(0) ? 0 : record.GetInt32(0); // Comets_id
            string name = record.IsDBNull(1) ? string.Empty : record.GetString(1); // Name
            object mass = record.IsDBNull(2) ? DBNull.Value : record.GetValue(2); // Mass
            object radius = record.IsDBNull(3) ? DBNull.Value : record.GetValue(3); // Radius
            string color = record.IsDBNull(4) ? string.Empty : record.GetString(4); // Color
            int sunnyspaceId = record.IsDBNull(5) ? 0 : record.GetInt32(5); // Sunnyspace_id

            dgw.Rows.Add(cometsId, name, mass, radius, color, sunnyspaceId, RoWState.ModifiedNew);
        }
        private void ReadOuterSpaceRow(DataGridView dgw, IDataRecord record)
        {
            int sunnyspaceId = record.IsDBNull(0) ? 0 : record.GetInt32(0); // Sunnyspace_id
            string name = record.IsDBNull(1) ? string.Empty : record.GetString(1); // Name
            long mass = record.IsDBNull(2) ? 0 : record.GetInt64(2); // Mass
            long radius = record.IsDBNull(3) ? 0 : record.GetInt64(3); // Radius
            string color = record.IsDBNull(4) ? string.Empty : record.GetString(4); // Color
            int sunId = record.IsDBNull(5) ? 0 : record.GetInt32(5); // Sun_id

            dgw.Rows.Add(sunnyspaceId, name, mass, radius, color, sunId, RoWState.ModifiedNew);
        }

        private void RefreshDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string queryString = $"select * from Planets";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReadSingleRow(dgw, reader);
                    }
                }
            }
        }
        private void RefreshSatelliteDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string queryString = $"select * from Satellites";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReadSatelliteRow(dgw, reader);
                    }
                }
            }
        }
        private void RefreshStarDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string queryString = $"select * from Sun";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ReadStarRow(dgw, reader);
                    }
                }
            }
        }
        private void RefreshMeteoriteDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string queryString = $"SELECT * FROM Meteorites";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bool found = false;
                        foreach (DataGridViewRow row in dgw.Rows)
                        {
                            if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == reader["Meteorites_id"].ToString())
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                         
                            DataGridViewRow newRow = new DataGridViewRow();
                            newRow.CreateCells(dgw);

                            
                            newRow.Cells[0].Value = reader["Meteorites_id"];
                            newRow.Cells[1].Value = reader["Name"];
                            newRow.Cells[2].Value = reader["Mass"];
                            newRow.Cells[3].Value = reader["Radius"];
                            newRow.Cells[4].Value = reader["Color"];
                            newRow.Cells[5].Value = reader["Sunnyspace_id"];

                            dgw.Rows.Add(newRow);
                        }
                    }
                }
            }
        }
        private void RefreshCometDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string queryString = $"SELECT * FROM Comets";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bool found = false;
                        foreach (DataGridViewRow row in dgw.Rows)
                        {
                            if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == reader["Comets_id"].ToString())
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                           
                            DataGridViewRow newRow = new DataGridViewRow();
                            newRow.CreateCells(dgw);

                          
                            newRow.Cells[0].Value = reader["Comets_id"];
                            newRow.Cells[1].Value = reader["Name"];
                            newRow.Cells[2].Value = reader["Mass"];
                            newRow.Cells[3].Value = reader["Radius"];
                            newRow.Cells[4].Value = reader["Color"];
                            newRow.Cells[5].Value = reader["Sunnyspace_id"];

                            dgw.Rows.Add(newRow);
                        }
                    }
                }
            }
        }

        private void RefreshOuterSpaceDataGrid(DataGridView dgw)
        {
            dgw.Rows.Clear();
            string queryString = "SELECT * FROM Sunnyspace";

            using (SqlCommand command = new SqlCommand(queryString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        bool found = false;
                        foreach (DataGridViewRow row in dgw.Rows)
                        {
                            if (row.Cells[0].Value != null && row.Cells[0].Value.ToString() == reader["Sunnyspace_id"].ToString())
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                          
                            DataGridViewRow newRow = new DataGridViewRow();
                            newRow.CreateCells(dgw);

               
                            newRow.Cells[0].Value = reader["Sunnyspace_id"];
                            newRow.Cells[1].Value = reader["Name"];
                            newRow.Cells[2].Value = reader["Mass"];
                            newRow.Cells[3].Value = reader["Radius"];
                            newRow.Cells[4].Value = reader["Color"];
                            newRow.Cells[5].Value = reader["Sun_id"];

                            dgw.Rows.Add(newRow);
                        }
                    }
                }
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripUser.Text = $"{_user.Login}: {_user.Status}";
            IsAdmin();
            CreatePlanetColumns();
            CreateSatelliteColumns();
            CreateStarColumns();
            CreateMeteoriteColumns();
            CreateCometColumns();
            CreateOuterSpaceColumns();
            RefreshDataGrid(dataGridView1);
            RefreshSatelliteDataGrid(dataGridViewSatellites);
            RefreshStarDataGrid(dataGridViewStars);
            RefreshMeteoriteDataGrid(dataGridViewMeteorites);
            RefreshCometDataGrid(dataGridViewComets);
            RefreshOuterSpaceDataGrid(dataGridViewOuterSpace);
            dataGridView1.Columns[9].Visible = false;
            dataGridViewSatellites.Columns[5].Visible = false;
            dataGridViewStars.Columns[5].Visible = false;
            dataGridViewMeteorites.Columns[6].Visible = false;
            dataGridViewComets.Columns[6].Visible = false;
            dataGridViewOuterSpace.Columns[6].Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[selectedRow];

                textBox_id.Text = row.Cells[0].Value.ToString();
                textBox_Name.Text = row.Cells[1].Value.ToString();
                textBox_Mass.Text = row.Cells[2].Value.ToString();
                textBox_Radius.Text = row.Cells[3].Value.ToString();
                textBox_Distance_Sun.Text = row.Cells[4].Value.ToString();
                textBox_Type_planet.Text = row.Cells[5].Value.ToString();
                textBox_Color.Text = row.Cells[6].Value.ToString();
                textBox_Sun_id.Text = row.Cells[7].Value.ToString();
                textBox_Satellite_id.Text = row.Cells[8].Value.ToString();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshDataGrid(dataGridView1);
            ClearPlanetsFields();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Add_Form add_Form = new Add_Form();
            add_Form.ShowDialog();
        }

        private void Search(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"select * from Planets where concat (Planet_id, Name, Mass, Radius, Distance_sun, Type_planet, Color, Sun_id, Satallite_id) like '%" + textBox_search.Text + "%'";

            SqlCommand com = new SqlCommand(searchString, dataBase.getConnection());

            dataBase.openConnection();

            SqlDataReader read = com.ExecuteReader();

            while (read.Read())
            {
                ReadSingleRow(dgw, read);
            }

            read.Close();
        }
        private void SearchSatellites(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"SELECT * FROM Satellites WHERE CONCAT(Satallite_id, Name, Mass, Radius, Color) LIKE '%{textBox_SearchSatellites.Text}%'";

            SqlCommand com = new SqlCommand(searchString, dataBase.getConnection());

            dataBase.openConnection();

            SqlDataReader read = com.ExecuteReader();

            while (read.Read())
            {
                ReadSatelliteRow(dgw, read);
            }

            read.Close();
        }
        private void SearchStars(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"SELECT * FROM Sun WHERE CONCAT(Sun_id, Name, Mass, Radius, Color) LIKE '%{textBox_SearchStars.Text}%'";

            SqlCommand com = new SqlCommand(searchString, dataBase.getConnection());

            dataBase.openConnection();

            SqlDataReader read = com.ExecuteReader();

            while (read.Read())
            {
                ReadStarRow(dgw, read); 
            }

            read.Close();
        }

        private void SearchMeteorites(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"SELECT * FROM Meteorites WHERE CONCAT(Meteorites_id, Name, Mass, Radius, Color, Sunnyspace_id) LIKE '%{textBox_SearchMeteorites.Text}%'";

            SqlCommand com = new SqlCommand(searchString, dataBase.getConnection());

            dataBase.openConnection();

            SqlDataReader read = com.ExecuteReader();

            while (read.Read())
            {
                ReadMeteoriteRow(dgw, read);
            }

            read.Close();
        }

        private void SearchComets(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"SELECT * FROM Comets WHERE CONCAT(Comets_id, Name, Mass, Radius, Color, Sunnyspace_id) LIKE '%{textBox_SearchComets.Text}%'";

            using (SqlCommand com = new SqlCommand(searchString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader read = com.ExecuteReader())
                {
                    while (read.Read())
                    {
                        ReadCometRow(dgw, read); 
                    }
                }
            }

            dataBase.closeConnection(); 
        }

        private void SearchOuterSpace(DataGridView dgw)
        {
            dgw.Rows.Clear();

            string searchString = $"SELECT * FROM Sunnyspace WHERE CONCAT(Sunnyspace_id, Name, Mass, Radius, Color, Sun_id) LIKE '%{textBox_SearchOuterSpace.Text}%'";

            using (SqlCommand com = new SqlCommand(searchString, dataBase.getConnection()))
            {
                dataBase.openConnection();

                using (SqlDataReader read = com.ExecuteReader())
                {
                    while (read.Read())
                    {
                       
                        ReadOuterSpaceRow(dgw, read);
                    }
                }
            }

            dataBase.closeConnection(); 
        }




        private void deleteRow()
        {
            int index = dataGridView1.CurrentCell.RowIndex;

            dataGridView1.Rows[index].Visible = false;

            if (dataGridView1.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
                dataGridView1.Rows[index].Cells[9].Value = RoWState.Deleted;
                return;
            }

            dataGridView1.Rows[index].Cells[9].Value = RoWState.Deleted;
        }

        private void deleteSatelliteRow()
        {
            int index = dataGridViewSatellites.CurrentCell.RowIndex;

            dataGridViewSatellites.Rows[index].Visible = false;

            if (dataGridViewSatellites.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
                dataGridViewSatellites.Rows[index].Cells[5].Value = RoWState.Deleted; 
                return;
            }

            dataGridViewSatellites.Rows[index].Cells[5].Value = RoWState.Deleted;
        }

        private void DeleteStarRow()
        {
            int index = dataGridViewStars.CurrentCell.RowIndex;

            dataGridViewStars.Rows[index].Visible = false;

            if (dataGridViewStars.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
            
                dataGridViewStars.Rows[index].Cells[5].Value = RoWState.Deleted;
                return;
            }

            dataGridViewStars.Rows[index].Cells[5].Value = RoWState.Deleted;
        }

        private void DeleteMeteoriteRow()
        {
            int index = dataGridViewMeteorites.CurrentCell.RowIndex;

            dataGridViewMeteorites.Rows[index].Visible = false;

            if (dataGridViewMeteorites.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
               
                dataGridViewMeteorites.Rows[index].Cells[6].Value = RoWState.Deleted;
                return;
            }

            dataGridViewMeteorites.Rows[index].Cells[6].Value = RoWState.Deleted;
        }

        private void DeleteCometRow()
        {
            int index = dataGridViewComets.CurrentCell.RowIndex;

            dataGridViewComets.Rows[index].Visible = false;

            if (dataGridViewComets.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
            
                dataGridViewComets.Rows[index].Cells[6].Value = RoWState.Deleted;
                return;
            }

            dataGridViewComets.Rows[index].Cells[6].Value = RoWState.Deleted;
        }

        private void DeleteOuterSpaceRow()
        {
            int index = dataGridViewOuterSpace.CurrentCell.RowIndex;

            dataGridViewOuterSpace.Rows[index].Visible = false;

            if (dataGridViewOuterSpace.Rows[index].Cells[0].Value.ToString() == string.Empty)
            {
             
                dataGridViewOuterSpace.Rows[index].Cells[6].Value = RoWState.Deleted;
                return;
            }

            dataGridViewOuterSpace.Rows[index].Cells[6].Value = RoWState.Deleted;
        }

        private void Update()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridView1.Rows.Count; index++)
            {
                var rowState = (RoWState)dataGridView1.Rows[index].Cells[9].Value;

                if (rowState == RoWState.Existed)
                    continue;


                if (rowState == RoWState.Deleted)
                {
                    if (dataGridView1.Rows[index].Cells[0].Value != null)
                    {
                        var Planet_id = Convert.ToInt32(dataGridView1.Rows[index].Cells[0].Value);
                        var deleteQuery = $"delete from Planets where Planet_id = {Planet_id}";

                        var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                        command.ExecuteNonQuery();
                    }
                }
                if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                {
                    var Planet_id = dataGridView1.Rows[index].Cells[0].Value.ToString();
                    var Name = dataGridView1.Rows[index].Cells[1].Value.ToString();
                    var Mass = dataGridView1.Rows[index].Cells[2].Value.ToString();
                    var Radius = dataGridView1.Rows[index].Cells[3].Value.ToString();
                    var Distance_sun = dataGridView1.Rows[index].Cells[4].Value.ToString();
                    var Type_planet = dataGridView1.Rows[index].Cells[5].Value.ToString();
                    var Color = dataGridView1.Rows[index].Cells[6].Value.ToString();
                    var Sun_id = dataGridView1.Rows[index].Cells[7].Value.ToString();
                    var Satallite_id = dataGridView1.Rows[index].Cells[8].Value.ToString();

                    var changeQuery = $"update Planets set Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Distance_sun = '{Distance_sun}', Type_planet = '{Type_planet}', Color = '{Color}', Sun_id = '{Sun_id}',  Satallite_id = '{Satallite_id}' where Planet_id = '{Planet_id}'";

                    var command = new SqlCommand(changeQuery, dataBase.getConnection());

                    command.ExecuteNonQuery();

                }
            }
            dataBase.closeConnection();
        }

        private void UpdateSatellites()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridViewSatellites.Rows.Count; index++)
            {
                var rowState = (RoWState)dataGridViewSatellites.Rows[index].Cells[5].Value; 

                if (rowState == RoWState.Existed)
                    continue;

                if (rowState == RoWState.Deleted)
                {
                    if (dataGridViewSatellites.Rows[index].Cells[0].Value != null)
                    {
                        var Satallite_id = Convert.ToInt32(dataGridViewSatellites.Rows[index].Cells[0].Value); 
                        var deleteQuery = $"DELETE FROM Satellites WHERE Satallite_id = {Satallite_id}";

                        var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                        command.ExecuteNonQuery();
                    }
                }

                if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                {
                    var Satallite_id = dataGridViewSatellites.Rows[index].Cells[0].Value.ToString(); 
                    var Name = dataGridViewSatellites.Rows[index].Cells[1].Value.ToString();
                    var Mass = dataGridViewSatellites.Rows[index].Cells[2].Value.ToString();
                    var Radius = dataGridViewSatellites.Rows[index].Cells[3].Value.ToString();
                    var Color = dataGridViewSatellites.Rows[index].Cells[4].Value.ToString();

                    var changeQuery = $"UPDATE Satellites SET Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Color = '{Color}' WHERE Satallite_id = '{Satallite_id}'";

                    var command = new SqlCommand(changeQuery, dataBase.getConnection());
                    command.ExecuteNonQuery();
                }
            }

            dataBase.closeConnection();
        }
        private void UpdateStars()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridViewStars.Rows.Count; index++)
            {
                var rowState = (RoWState)dataGridViewStars.Rows[index].Cells[5].Value; 

                if (rowState == RoWState.Existed)
                    continue;

                if (rowState == RoWState.Deleted)
                {
                    if (dataGridViewStars.Rows[index].Cells[0].Value != null)
                    {
                        var Sun_id = Convert.ToInt32(dataGridViewStars.Rows[index].Cells[0].Value); 
                        var deleteQuery = $"DELETE FROM Sun WHERE Sun_id = {Sun_id}";

                        var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                        command.ExecuteNonQuery();
                    }
                }

                if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                {
                    var Sun_id = dataGridViewStars.Rows[index].Cells[0].Value.ToString();
                    var Name = dataGridViewStars.Rows[index].Cells[1].Value.ToString();
                    var Mass = dataGridViewStars.Rows[index].Cells[2].Value.ToString();
                    var Radius = dataGridViewStars.Rows[index].Cells[3].Value.ToString();
                    var Color = dataGridViewStars.Rows[index].Cells[4].Value.ToString();

                    var changeQuery = $"UPDATE Sun SET Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Color = '{Color}' WHERE Sun_id = '{Sun_id}'";

                    var command = new SqlCommand(changeQuery, dataBase.getConnection());
                    command.ExecuteNonQuery();
                }
            }

            dataBase.closeConnection();
        }
        private void UpdateMeteorites()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridViewMeteorites.Rows.Count; index++)
            {
                if (dataGridViewMeteorites.Rows[index].Cells[6].Value != null)
                {
                    var rowState = (RoWState)dataGridViewMeteorites.Rows[index].Cells[6].Value;

                    if (rowState == RoWState.Deleted)
                    {
                        if (dataGridViewMeteorites.Rows[index].Cells[0].Value != null &&
                            int.TryParse(dataGridViewMeteorites.Rows[index].Cells[0].Value.ToString(), out int Meteorites_id))
                        {
                            var deleteQuery = $"DELETE FROM Meteorites WHERE Meteorites_id = {Meteorites_id}";

                            var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }

                    if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                    {
                        if (dataGridViewMeteorites.Rows[index].Cells[0].Value != null &&
                            dataGridViewMeteorites.Rows[index].Cells[1].Value != null &&
                            dataGridViewMeteorites.Rows[index].Cells[2].Value != null &&
                            dataGridViewMeteorites.Rows[index].Cells[3].Value != null &&
                            dataGridViewMeteorites.Rows[index].Cells[4].Value != null &&
                            dataGridViewMeteorites.Rows[index].Cells[5].Value != null &&
                            int.TryParse(dataGridViewMeteorites.Rows[index].Cells[0].Value.ToString(), out int Meteorites_id) &&
                            int.TryParse(dataGridViewMeteorites.Rows[index].Cells[2].Value.ToString(), out int Mass) &&
                            int.TryParse(dataGridViewMeteorites.Rows[index].Cells[3].Value.ToString(), out int Radius) &&
                            int.TryParse(dataGridViewMeteorites.Rows[index].Cells[5].Value.ToString(), out int Sunnyspace_id))
                        {
                            var Name = dataGridViewMeteorites.Rows[index].Cells[1].Value.ToString();
                            var Color = dataGridViewMeteorites.Rows[index].Cells[4].Value.ToString();

                            var changeQuery = $"UPDATE Meteorites SET Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Color = '{Color}', Sunnyspace_id = '{Sunnyspace_id}' WHERE Meteorites_id = '{Meteorites_id}'";

                            var command = new SqlCommand(changeQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            dataBase.closeConnection();
        }

        private void UpdateComets()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridViewComets.Rows.Count; index++)
            {
                if (dataGridViewComets.Rows[index].Cells[6].Value != null)
                {
                    var rowState = (RoWState)dataGridViewComets.Rows[index].Cells[6].Value;

                    if (rowState == RoWState.Deleted)
                    {
                        if (dataGridViewComets.Rows[index].Cells[0].Value != null &&
                            int.TryParse(dataGridViewComets.Rows[index].Cells[0].Value.ToString(), out int Comets_id))
                        {
                            var deleteQuery = $"DELETE FROM Comets WHERE Comets_id = {Comets_id}";

                            var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }

                    if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                    {
                        if (dataGridViewComets.Rows[index].Cells[0].Value != null &&
                            dataGridViewComets.Rows[index].Cells[1].Value != null &&
                            dataGridViewComets.Rows[index].Cells[2].Value != null &&
                            dataGridViewComets.Rows[index].Cells[3].Value != null &&
                            dataGridViewComets.Rows[index].Cells[4].Value != null &&
                            dataGridViewComets.Rows[index].Cells[5].Value != null &&
                            int.TryParse(dataGridViewComets.Rows[index].Cells[0].Value.ToString(), out int Comets_id) &&
                            int.TryParse(dataGridViewComets.Rows[index].Cells[2].Value.ToString(), out int Mass) &&
                            int.TryParse(dataGridViewComets.Rows[index].Cells[3].Value.ToString(), out int Radius) &&
                            int.TryParse(dataGridViewComets.Rows[index].Cells[5].Value.ToString(), out int Sunnyspace_id))
                        {
                            var Name = dataGridViewComets.Rows[index].Cells[1].Value.ToString();
                            var Color = dataGridViewComets.Rows[index].Cells[4].Value.ToString();

                            var changeQuery = $"UPDATE Comets SET Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Color = '{Color}', Sunnyspace_id = '{Sunnyspace_id}' WHERE Comets_id = '{Comets_id}'";

                            var command = new SqlCommand(changeQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            dataBase.closeConnection();
        }

        private void UpdateOuterSpace()
        {
            dataBase.openConnection();

            for (int index = 0; index < dataGridViewOuterSpace.Rows.Count; index++)
            {
                if (dataGridViewOuterSpace.Rows[index].Cells[6].Value != null)
                {
                    var rowState = (RoWState)dataGridViewOuterSpace.Rows[index].Cells[6].Value;

                    if (rowState == RoWState.Deleted)
                    {
                        if (dataGridViewOuterSpace.Rows[index].Cells[0].Value != null &&
                            int.TryParse(dataGridViewOuterSpace.Rows[index].Cells[0].Value.ToString(), out int Sunnyspace_id))
                        {
                            var deleteQuery = $"DELETE FROM Sunnyspace WHERE Sunnyspace_id = {Sunnyspace_id}";

                            var command = new SqlCommand(deleteQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }

                    if (rowState == RoWState.Modified || rowState == RoWState.ModifiedNew)
                    {
                        if (dataGridViewOuterSpace.Rows[index].Cells[0].Value != null &&
                            dataGridViewOuterSpace.Rows[index].Cells[1].Value != null &&
                            dataGridViewOuterSpace.Rows[index].Cells[2].Value != null &&
                            dataGridViewOuterSpace.Rows[index].Cells[3].Value != null &&
                            dataGridViewOuterSpace.Rows[index].Cells[4].Value != null &&
                            dataGridViewOuterSpace.Rows[index].Cells[5].Value != null &&
                            int.TryParse(dataGridViewOuterSpace.Rows[index].Cells[0].Value.ToString(), out int Sunnyspace_id) &&
                            int.TryParse(dataGridViewOuterSpace.Rows[index].Cells[2].Value.ToString(), out int Mass) &&
                            int.TryParse(dataGridViewOuterSpace.Rows[index].Cells[3].Value.ToString(), out int Radius) &&
                            int.TryParse(dataGridViewOuterSpace.Rows[index].Cells[5].Value.ToString(), out int Sun_id))
                        {
                            var Name = dataGridViewOuterSpace.Rows[index].Cells[1].Value.ToString();
                            var Color = dataGridViewOuterSpace.Rows[index].Cells[4].Value.ToString();

                            var changeQuery = $"UPDATE Sunnyspace SET Name = '{Name}', Mass = '{Mass}', Radius = '{Radius}', Color = '{Color}', Sun_id = '{Sun_id}' WHERE Sunnyspace_id = '{Sunnyspace_id}'";

                            var command = new SqlCommand(changeQuery, dataBase.getConnection());
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            dataBase.closeConnection();
        }


        private void textBox_search_TextChanged(object sender, EventArgs e)
        {
            Search(dataGridView1);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteRow();
            ClearPlanetsFields();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Update();
            ClearPlanetsFields();
        }

        private void Change()
        {
            var selectedRowIndex = dataGridView1.CurrentCell.RowIndex;

            int Planet_Id;
            var Name = textBox_Name.Text;
            int Mass;
            int Radius;
            int Distance_sun;
            var Type_planet = textBox_Type_planet.Text;
            var Color = textBox_Color.Text;
            int Sun_id;
            int Satallite_id;

     
            if (dataGridView1.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridView1.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Planet_Id))
            {
                if (int.TryParse(textBox_id.Text, out Planet_Id) &&
                    int.TryParse(textBox_Mass.Text, out Mass) &&
                    int.TryParse(textBox_Radius.Text, out Radius) &&
                    int.TryParse(textBox_Distance_Sun.Text, out Distance_sun) &&
                    int.TryParse(textBox_Sun_id.Text, out Sun_id) &&
                    int.TryParse(textBox_Satellite_id.Text, out Satallite_id))
                {
                   
                    if (IsDuplicatePlanet(Name))
                    {
                        MessageBox.Show("Це призведе до дублікату, що спричинить збій Бази Даних. Будь ласка, введіть дані які не будуть повторюватися.");
                        return;
                    }

                    dataGridView1.Rows[selectedRowIndex].SetValues(Planet_Id, Name, Mass, Radius, Distance_sun, Type_planet, Color, Sun_id, Satallite_id);
                    dataGridView1.Rows[selectedRowIndex].Cells[9].Value = RoWState.Modified;
                }
                else
                {
                    MessageBox.Show("Повинен бути цифровий формат!");
                }
            }
        }

        private void ChangeSatellite()
        {
            var selectedRowIndex = dataGridViewSatellites.CurrentCell.RowIndex;

            int Satallite_Id;
            var Name = textBox_Satallite_Name.Text;
            int Mass;
            int Radius;
            var Color = textBox_Satallite_Color.Text;

            if (dataGridViewSatellites.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridViewSatellites.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Satallite_Id))
            {
                if (int.TryParse(textBox_Satallite_id.Text, out Satallite_Id) &&
                    int.TryParse(textBox_Satallite_Mass.Text, out Mass) &&
                    int.TryParse(textBox_Satallite_Radius.Text, out Radius))
                {
                    dataGridViewSatellites.Rows[selectedRowIndex].SetValues(Satallite_Id, Name, Mass, Radius, Color);
                    dataGridViewSatellites.Rows[selectedRowIndex].Cells[6].Value = RoWState.Modified; 
                }
                else
                {
                    MessageBox.Show("Повинен бути числовий формат!");
                }
            }
        }

        private void ChangeStar()
        {
            var selectedRowIndex = dataGridViewStars.CurrentCell.RowIndex;

            int Sun_Id;
            var Name = textBox_Name_Star.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_Star.Text;

            if (dataGridViewStars.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridViewStars.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Sun_Id))
            {
                if (int.TryParse(textBox_Id_Star.Text, out Sun_Id) &&
                    int.TryParse(textBox_Mass_Star.Text, out Mass) &&
                    int.TryParse(textBox_Radius_Star.Text, out Radius))
                {
                    dataGridViewStars.Rows[selectedRowIndex].SetValues(Sun_Id, Name, Mass, Radius, Color);
                    dataGridViewStars.Rows[selectedRowIndex].Cells[5].Value = RoWState.Modified;
                }
                else
                {
                    MessageBox.Show("Повинен бути числовий формат!");
                }
            }
        }

        private void ChangeMeteorite()
        {
            var selectedRowIndex = dataGridViewMeteorites.CurrentCell.RowIndex;

            int Meteorites_id;
            var Name = textBox_Name_Meteorite.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_Meteorite.Text;
            int Sunnyspace_id;

            if (dataGridViewMeteorites.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridViewMeteorites.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Meteorites_id))
            {
                if (int.TryParse(textBox_Id_Meteorite.Text, out Meteorites_id) &&
                    int.TryParse(textBox_Mass_Meteorite.Text, out Mass) &&
                    int.TryParse(textBox_Radius_Meteorite.Text, out Radius) &&
                    int.TryParse(textBox_IDPROSTIR_Meteorite.Text, out Sunnyspace_id))
                {
                    dataGridViewMeteorites.Rows[selectedRowIndex].SetValues(Meteorites_id, Name, Mass, Radius, Color, Sunnyspace_id);
                    dataGridViewMeteorites.Rows[selectedRowIndex].Cells[6].Value = RoWState.Modified; 
                }
                else
                {
                    MessageBox.Show("Повинен бути числовий формат!");
                }
            }
        }

        private void ChangeComet()
        {
            var selectedRowIndex = dataGridViewComets.CurrentCell.RowIndex;

            int Comets_id;
            var Name = textBox_Name_Comets.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_Comets.Text;
            int Sunnyspace_id;

            if (dataGridViewComets.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridViewComets.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Comets_id))
            {
                if (int.TryParse(textBox_Id_Comets.Text, out Comets_id) &&
                    int.TryParse(textBox_Mass_Comets.Text, out Mass) &&
                    int.TryParse(textBox_Radius_Comets.Text, out Radius) &&
                    int.TryParse(textBox_IDPROSTIR_Comets.Text, out Sunnyspace_id))
                {
                    dataGridViewComets.Rows[selectedRowIndex].SetValues(Comets_id, Name, Mass, Radius, Color, Sunnyspace_id);
                    dataGridViewComets.Rows[selectedRowIndex].Cells[6].Value = RoWState.Modified; // Assuming status column index is 6
                }
                else
                {
                    MessageBox.Show("Повинен бути числовий формат!");
                }
            }
        }

        private void ChangeOuterSpace()
        {
            var selectedRowIndex = dataGridViewOuterSpace.CurrentCell.RowIndex;

            int Sunnyspace_id;
            var Name = textBox_Name_OuterSpace.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_OuterSpace.Text;
            int Sun_id;

            if (dataGridViewOuterSpace.Rows[selectedRowIndex].Cells[0].Value != null &&
                int.TryParse(dataGridViewOuterSpace.Rows[selectedRowIndex].Cells[0].Value.ToString(), out Sunnyspace_id))
            {
                if (int.TryParse(textBox_SunnySpace_id_OuterSpace.Text, out Sunnyspace_id) &&
                    int.TryParse(textBox_Mass_OuterSpace.Text, out Mass) &&
                    int.TryParse(textBox_Radius_OuterSpace.Text, out Radius) &&
                    int.TryParse(textBox_IDPROSTIR_OuterSpace.Text, out Sun_id))
                {
                    dataGridViewOuterSpace.Rows[selectedRowIndex].SetValues(Sunnyspace_id, Name, Mass, Radius, Color, Sun_id);
                    dataGridViewOuterSpace.Rows[selectedRowIndex].Cells[6].Value = RoWState.Modified;
                }
                else
                {
                    MessageBox.Show("Повинен бути числовий формат!");
                }
            }
        }



        private bool IsDuplicatePlanet(string planetName)
        {
        
            string queryString = $"SELECT COUNT(*) FROM Planets WHERE Name = '{planetName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }

        private bool IsDuplicateSatellite(string satelliteName)
        {
           
            string queryString = $"SELECT COUNT(*) FROM Satellites WHERE Name = '{satelliteName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }

        private bool IsDuplicateStar(string sunName)
        {
     
            string queryString = $"SELECT COUNT(*) FROM Sun WHERE Name = '{sunName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }

        private bool IsDuplicateMeteorite(string meteoriteName)
        {
      
            string queryString = $"SELECT COUNT(*) FROM Meteorites WHERE Name = '{meteoriteName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }

        private bool IsDuplicateComet(string cometName)
        {
           
            string queryString = $"SELECT COUNT(*) FROM Comets WHERE Name = '{cometName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }

        private bool IsDuplicateOuterSpace(string SunnyspaceName)
        {
            
            string queryString = $"SELECT COUNT(*) FROM Sunnyspace WHERE Name = '{SunnyspaceName}'";
            SqlCommand command = new SqlCommand(queryString, dataBase.getConnection());

            dataBase.openConnection();

            int count = (int)command.ExecuteScalar();
            dataBase.closeConnection();

            return count > 0;
        }



        private void btnChange_Click(object sender, EventArgs e)
        {
            Change();
            ClearPlanetsFields();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearPlanetsFields();
        }

        private void dataGridViewSatellites_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridViewSatellites.Rows[selectedRow];

           
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    Console.WriteLine($"Cell {i}: {row.Cells[i].Value}");
                }

                textBox_Satallite_id.Text = row.Cells[0].Value.ToString();
                textBox_Satallite_Name.Text = row.Cells[1].Value.ToString();
                textBox_Satallite_Mass.Text = row.Cells[2].Value.ToString();
                textBox_Satallite_Radius.Text = row.Cells[3].Value.ToString();
                textBox_Satallite_Color.Text = row.Cells[4].Value.ToString();
            
            }
        }

        private void btnRefresh_Satallites_Click(object sender, EventArgs e)
        {
            RefreshSatelliteDataGrid(dataGridViewSatellites);
            ClearSatelliteFields();
        }

        private void btnNewSatellite_Click(object sender, EventArgs e)
        {
            AddSatellite_Form addSatelliteForm = new AddSatellite_Form();
            addSatelliteForm.ShowDialog();

        }

        private void textBox_SearchSatellites_TextChanged(object sender, EventArgs e)
        {
            SearchSatellites(dataGridViewSatellites);
        }

        private void btnDeleteSatellite_Click(object sender, EventArgs e)
        {
            deleteSatelliteRow();
            ClearSatelliteFields();
        }

        private void btnUpdateSatellites_Click(object sender, EventArgs e)
        {
            UpdateSatellites();
            ClearSatelliteFields();
        }

        private void btnChangeSatellite_Click(object sender, EventArgs e)
        {
            ChangeSatellite();
            ClearSatelliteFields();
        }

        private void btnClearSatellites_Click(object sender, EventArgs e)
        {
            ClearSatelliteFields();
        }

        private void btnClear_Click_1(object sender, EventArgs e)
        {
            ClearPlanetsFields();
        }

        private void dataGridViewStars_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridViewStars.Rows[selectedRow];

           
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    Console.WriteLine($"Cell {i}: {row.Cells[i].Value}");
                }

                textBox_Id_Star.Text = row.Cells[0].Value.ToString();
                textBox_Name_Star.Text = row.Cells[1].Value.ToString();
                textBox_Mass_Star.Text = row.Cells[2].Value.ToString();
                textBox_Radius_Star.Text = row.Cells[3].Value.ToString();
                textBox_Color_Star.Text = row.Cells[4].Value.ToString();
            }
        }

        private void btnRefresh_Stars_Click(object sender, EventArgs e)
        {
            RefreshStarDataGrid(dataGridViewStars);
            ClearStarFields();
        }

        private void btnNewStar_Click(object sender, EventArgs e)
        {
            AddStar_Form addStarForm = new AddStar_Form();
            addStarForm.ShowDialog();
        }

        private void textBox_SearchStars_TextChanged(object sender, EventArgs e)
        {
            SearchStars(dataGridViewStars);
        }

        private void btnDeleteStar_Click(object sender, EventArgs e)
        {
            DeleteStarRow();
            ClearStarFields();
        }

        private void btnUpdateStar_Click(object sender, EventArgs e)
        {
            UpdateStars();
            ClearStarFields();
        }

        private void btnChangeStar_Click(object sender, EventArgs e)
        {
            ChangeStar();
            ClearStarFields();
        }

        private void btnClearStars_Click(object sender, EventArgs e)
        {
            ClearStarFields();
        }

        private void dataGridViewMeteorites_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridViewMeteorites.Rows[selectedRow];

                if (row.Cells[0].Value != null)
                    textBox_Id_Meteorite.Text = row.Cells[0].Value.ToString();
                else
                    textBox_Id_Meteorite.Text = string.Empty;

                if (row.Cells[1].Value != null)
                    textBox_Name_Meteorite.Text = row.Cells[1].Value.ToString();
                else
                    textBox_Name_Meteorite.Text = string.Empty;
                if (row.Cells[2].Value != null)
                    textBox_Mass_Meteorite.Text = row.Cells[2].Value.ToString();
                else
                    textBox_Mass_Meteorite.Text = string.Empty;
                if (row.Cells[3].Value != null)
                    textBox_Radius_Meteorite.Text = row.Cells[3].Value.ToString();
                else
                    textBox_Radius_Meteorite.Text = string.Empty;
                if (row.Cells[4].Value != null)
                    textBox_Color_Meteorite.Text = row.Cells[4].Value.ToString();
                else
                    textBox_Color_Meteorite.Text = string.Empty;
                if (row.Cells[5].Value != null)
                    textBox_IDPROSTIR_Meteorite.Text = row.Cells[5].Value.ToString();
                else
                    textBox_IDPROSTIR_Meteorite.Text = string.Empty;
            }
        }

        private void btnRefresh_Meteorites_Click(object sender, EventArgs e)
        {
            RefreshMeteoriteDataGrid(dataGridViewMeteorites);
            ClearMeteoriteFields();
        }

        private void btnNewMeteorite_Click(object sender, EventArgs e)
        {
            AddMeteorite addMeteoriteForm = new AddMeteorite();
            addMeteoriteForm.ShowDialog();
        }

        private void textBox_SearchMeteorites_TextChanged(object sender, EventArgs e)
        {
            SearchMeteorites(dataGridViewMeteorites);
        }

        private void btnDeleteMeteorite_Click(object sender, EventArgs e)
        {
            DeleteMeteoriteRow();
            ClearMeteoriteFields();
        }

        private void btnUpdateMeteorite_Click(object sender, EventArgs e)
        {
            UpdateMeteorites();
            ClearMeteoriteFields();
        }

        private void btnChangeMeteorite_Click(object sender, EventArgs e)
        {
            ChangeMeteorite();
            ClearMeteoriteFields();
        }

        private void btnClearMeteorite_Click(object sender, EventArgs e)
        {
            ClearMeteoriteFields();
        }

        private void dataGridViewComets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridViewComets.Rows[selectedRow];

                if (row.Cells[0].Value != null)
                    textBox_Id_Comets.Text = row.Cells[0].Value.ToString();
                else
                    textBox_Id_Comets.Text = string.Empty;

                if (row.Cells[1].Value != null)
                    textBox_Name_Comets.Text = row.Cells[1].Value.ToString();
                else
                    textBox_Name_Comets.Text = string.Empty;

                if (row.Cells[2].Value != null)
                    textBox_Mass_Comets.Text = row.Cells[2].Value.ToString();
                else
                    textBox_Mass_Comets.Text = string.Empty;

                if (row.Cells[3].Value != null)
                    textBox_Radius_Comets.Text = row.Cells[3].Value.ToString();
                else
                    textBox_Radius_Comets.Text = string.Empty;

                if (row.Cells[4].Value != null)
                    textBox_Color_Comets.Text = row.Cells[4].Value.ToString();
                else
                    textBox_Color_Comets.Text = string.Empty;

                if (row.Cells[5].Value != null)
                    textBox_IDPROSTIR_Comets.Text = row.Cells[5].Value.ToString();
                else
                    textBox_IDPROSTIR_Comets.Text = string.Empty;
            }
        }

        private void btnRefresh_Comets_Click(object sender, EventArgs e)
        {
            RefreshCometDataGrid(dataGridViewComets);
            ClearCometFields();
        }

        private void btnNewComets_Click(object sender, EventArgs e)
        {
            AddComets addCometsForm = new AddComets();
            addCometsForm.ShowDialog();
        }

        private void textBox_SearchComets_TextChanged(object sender, EventArgs e)
        {
            SearchComets(dataGridViewComets);
        }

        private void btnDeleteComets_Click(object sender, EventArgs e)
        {
            DeleteCometRow();
            ClearCometFields();
        }

        private void btnUpdateComets_Click(object sender, EventArgs e)
        {
            UpdateComets();
            ClearCometFields();
        }

        private void btnChangeComets_Click(object sender, EventArgs e)
        {
            ChangeComet();
            ClearCometFields();
        }

        private void btnClearComets_Click(object sender, EventArgs e)
        {
            ClearCometFields();
        }

        private void dataGridViewOuterSpace_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridViewOuterSpace.Rows[selectedRow];

                if (row.Cells[0].Value != null)
                    textBox_SunnySpace_id_OuterSpace.Text = row.Cells[0].Value.ToString();
                else
                    textBox_SunnySpace_id_OuterSpace.Text = string.Empty;

                if (row.Cells[1].Value != null)
                    textBox_Name_OuterSpace.Text = row.Cells[1].Value.ToString();
                else
                    textBox_Name_OuterSpace.Text = string.Empty;

                if (row.Cells[2].Value != null)
                    textBox_Mass_OuterSpace.Text = row.Cells[2].Value.ToString();
                else
                    textBox_Mass_OuterSpace.Text = string.Empty;

                if (row.Cells[3].Value != null)
                    textBox_Radius_OuterSpace.Text = row.Cells[3].Value.ToString();
                else
                    textBox_Radius_OuterSpace.Text = string.Empty;

                if (row.Cells[4].Value != null)
                    textBox_Color_OuterSpace.Text = row.Cells[4].Value.ToString();
                else
                    textBox_Color_OuterSpace.Text = string.Empty;

                if (row.Cells[5].Value != null)
                    textBox_IDPROSTIR_OuterSpace.Text = row.Cells[5].Value.ToString();
                else
                    textBox_IDPROSTIR_OuterSpace.Text = string.Empty;
            }
        }

        private void btnRefresh_OuterSpace_Click(object sender, EventArgs e)
        {
            RefreshOuterSpaceDataGrid(dataGridViewOuterSpace);
            ClearOuterSpaceFields();
        }

        private void btnNewOuterSpace_Click(object sender, EventArgs e)
        {
            OuterSpace OuterSpaceForm = new OuterSpace();
            OuterSpaceForm.ShowDialog();
        }

        private void textBox_SearchOuterSpace_TextChanged(object sender, EventArgs e)
        {
            SearchOuterSpace(dataGridViewOuterSpace);
        }

        private void btnDeleteOuterSpace_Click(object sender, EventArgs e)
        {
            DeleteOuterSpaceRow();
            ClearOuterSpaceFields();
        }

        private void btnUpdateOuterSpace_Click(object sender, EventArgs e)
        {
            UpdateOuterSpace();
            ClearOuterSpaceFields();
        }

        private void btnChangeOuterSpace_Click(object sender, EventArgs e)
        {
            ChangeOuterSpace();
            ClearOuterSpaceFields();
        }

        private void btnClearOuterSpace_Click(object sender, EventArgs e)
        {
            ClearOuterSpaceFields();
        }

        private void CreateBackup_Click(object sender, EventArgs e)
        {
            BackUpOuterSpace backUpOuterSpace = new BackUpOuterSpace();
            backUpOuterSpace.ShowDialog();
        }

        private void керуванняToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Admin admin = new Admin();
            admin.ShowDialog();
        }
    }
}

 