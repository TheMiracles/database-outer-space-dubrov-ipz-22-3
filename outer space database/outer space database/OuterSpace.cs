﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace outer_space_database
{
    public partial class OuterSpace : Form
    {
        DataBase dataBase = new DataBase();
        public OuterSpace()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnAddOuterSpace_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Sunnyspace_id;
            var Name = textBox_Name_OuterSpace.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_OuterSpace.Text;
            int Sun_id;

            if (int.TryParse(textBox_SunnySpace_id_OuterSpace.Text, out Sunnyspace_id) &&
                int.TryParse(textBox_Mass_OuterSpace.Text, out Mass) &&
                int.TryParse(textBox_Radius_OuterSpace.Text, out Radius) &&
                int.TryParse(textBox_IDPROSTIR_OuterSpace.Text, out Sun_id))
            {
                var addQuery = $"INSERT INTO Sunnyspace (Sunnyspace_id, Name, Mass, Radius, Color, Sun_id) VALUES ('{Sunnyspace_id}', '{Name}', '{Mass}', '{Radius}', '{Color}', '{Sun_id}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Дані про новий космічний об'єкт успішно додано!", "Успішно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();

        }
    }
}
