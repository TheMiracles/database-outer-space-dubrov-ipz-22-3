﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace outer_space_database
{
    public partial class AddSatellite_Form : Form
    {
        DataBase dataBase = new DataBase();

        public AddSatellite_Form()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void AddSatellite_Form_Load(object sender, EventArgs e)
        {

        }

        private void btnAddSatellite_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Satallite_Id;
            var Name = textBox_SatelliteName.Text;
            int Mass;
            int Radius;
            var Color = textBox_SatelliteColor.Text;

            if (int.TryParse(textBox_Satallite_Id2.Text, out Satallite_Id) &&
                int.TryParse(textBox_SatelliteMass.Text, out Mass) &&
                int.TryParse(textBox_SatelliteRadius.Text, out Radius))
            {
                var addQuery = $"INSERT INTO Satellites (Satallite_Id, Name, Mass, Radius, Color) VALUES ('{Satallite_Id}', '{Name}', '{Mass}', '{Radius}', '{Color}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Дані про супутник успішно додано!", "Успішно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();
        }
    }
}
