﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace outer_space_database
{
    public partial class AddMeteorite : Form
    {
        DataBase dataBase = new DataBase();

        public AddMeteorite()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnAddMeteorite_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Meteorites_id;
            var Name = textBox_MeteoriteName.Text;
            int Mass;
            int Radius;
            var Color = textBox_MeteoriteColor.Text;
            int Sunnyspace_id;

            if (int.TryParse(textBox_Meteorite_Id2.Text, out Meteorites_id) &&
                int.TryParse(textBox_MeteoriteMass.Text, out Mass) &&
                int.TryParse(textBox_MeteoriteRadius.Text, out Radius) &&
                int.TryParse(textBoxMeteorite_ProstirID.Text, out Sunnyspace_id))
            {
                var addQuery = $"INSERT INTO Meteorites (Meteorites_id, Name, Mass, Radius, Color, Sunnyspace_id) VALUES ('{Meteorites_id}', '{Name}', '{Mass}', '{Radius}', '{Color}', '{Sunnyspace_id}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Дані про новий метеорит успішно додано!", "Успішно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();

        }
    }
}
