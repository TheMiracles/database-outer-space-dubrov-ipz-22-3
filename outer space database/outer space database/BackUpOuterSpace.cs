﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;

namespace outer_space_database
{
    public partial class BackUpOuterSpace : Form
    {
        SqlConnection con = new SqlConnection("server=DESKTOP-5JJDRQF; database = solarsystem2; integrated security = true");
        public BackUpOuterSpace()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnCreateBackUp_Click(object sender, EventArgs e)
        {
            string database = con.Database.ToString();
            if(textBox_Location.Text == string.Empty)
            {
                MessageBox.Show("Будьласка нажміть Enter, щоб обрати шлях для файлу");
            }
            else
            {
                string cmd = "BACKUP DATABASE [" + database + "] TO DISK= '" + textBox_Location.Text + "\\" + "database" + "-" + DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".bak '";
                con.Open();
                SqlCommand command = new SqlCommand(cmd,con);
                command.ExecuteNonQuery();
                MessageBox.Show("DataBase Backup успішно створено!!!");
                con.Close();
                btnCreateBackUp.Enabled = false;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                textBox_Location.Text = dlg.SelectedPath;
                btnCreateBackUp.Enabled = true;
            }
        }

        private void btnBrowse2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "SQL SERVER database backup files (*.bak)|*.bak";
            dlg.Title = "Database restore";
            if(dlg.ShowDialog() == DialogResult.OK)
            {
                textBox_Location2.Text = dlg.FileName;
                btnRestoreBackUp.Enabled = true;

            }
        }

        private void btnRestoreBackUp_Click(object sender, EventArgs e)
        {
            string database = con.Database.ToString();
            con.Open();

            try
            {
      string str1 = string.Format("ALTER DATABASE [" + database + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                SqlCommand cmd1 = new SqlCommand(str1,con);
                cmd1.ExecuteNonQuery();

                string str2 = "USE MASTER RESTORE DATABASE [" + database + "] FROM DISK='" + textBox_Location2.Text + "' WITH REPLACE; ";
                SqlCommand cmd2 =new SqlCommand(str2,con);
                cmd2.ExecuteNonQuery();

                string str3 = string.Format("ALTER DATABASE [" + database + "] SET MULTI_USER");
                SqlCommand cmd3 = new SqlCommand(str3,con);
                cmd3.ExecuteNonQuery();
                MessageBox.Show("Відновлення бази даних виконано успішно!");
                con.Close();
            }
            catch
            {
          

            }
        }
    }
}
