﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace outer_space_database
{
    public partial class AddComets : Form
    {
        DataBase dataBase = new DataBase();
        public AddComets()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void btnAddComets_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Comets_id;
            var Name = textBox_Name_Comets.Text;
            int Mass;
            int Radius;
            var Color = textBox_Color_Comets.Text;
            int Sunnyspace_id;

            if (int.TryParse(textBox_Id_Comets.Text, out Comets_id) &&
                int.TryParse(textBox_Mass_Comets.Text, out Mass) &&
                int.TryParse(textBox_Radius_Comets.Text, out Radius) &&
                int.TryParse(textBox_IDPROSTIR_Comets.Text, out Sunnyspace_id))
            {
                var addQuery = $"INSERT INTO Comets (Comets_id, Name, Mass, Radius, Color, Sunnyspace_id) VALUES ('{Comets_id}', '{Name}', '{Mass}', '{Radius}', '{Color}', '{Sunnyspace_id}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Дані про нову комету успішно додано!", "Успішно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();
        }
    }
}
