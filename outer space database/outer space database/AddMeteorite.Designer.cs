﻿namespace outer_space_database
{
    partial class AddMeteorite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMeteorite));
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_MeteoriteColor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_MeteoriteRadius = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_MeteoriteMass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_MeteoriteName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Meteorite_Id2 = new System.Windows.Forms.TextBox();
            this.btnAddMeteorite = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxMeteorite_ProstirID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 243);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 50;
            this.label5.Text = "Колір:";
            // 
            // textBox_MeteoriteColor
            // 
            this.textBox_MeteoriteColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_MeteoriteColor.Location = new System.Drawing.Point(272, 243);
            this.textBox_MeteoriteColor.Name = "textBox_MeteoriteColor";
            this.textBox_MeteoriteColor.Size = new System.Drawing.Size(196, 22);
            this.textBox_MeteoriteColor.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 48;
            this.label4.Text = "Радіус:";
            // 
            // textBox_MeteoriteRadius
            // 
            this.textBox_MeteoriteRadius.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_MeteoriteRadius.Location = new System.Drawing.Point(272, 215);
            this.textBox_MeteoriteRadius.Name = "textBox_MeteoriteRadius";
            this.textBox_MeteoriteRadius.Size = new System.Drawing.Size(196, 22);
            this.textBox_MeteoriteRadius.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 46;
            this.label3.Text = "Маса:";
            // 
            // textBox_MeteoriteMass
            // 
            this.textBox_MeteoriteMass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_MeteoriteMass.Location = new System.Drawing.Point(272, 187);
            this.textBox_MeteoriteMass.Name = "textBox_MeteoriteMass";
            this.textBox_MeteoriteMass.Size = new System.Drawing.Size(196, 22);
            this.textBox_MeteoriteMass.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "Назва:";
            // 
            // textBox_MeteoriteName
            // 
            this.textBox_MeteoriteName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_MeteoriteName.Location = new System.Drawing.Point(272, 159);
            this.textBox_MeteoriteName.Name = "textBox_MeteoriteName";
            this.textBox_MeteoriteName.Size = new System.Drawing.Size(196, 22);
            this.textBox_MeteoriteName.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(243, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 42;
            this.label1.Text = "ID:";
            // 
            // textBox_Meteorite_Id2
            // 
            this.textBox_Meteorite_Id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Meteorite_Id2.Location = new System.Drawing.Point(272, 131);
            this.textBox_Meteorite_Id2.Name = "textBox_Meteorite_Id2";
            this.textBox_Meteorite_Id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Meteorite_Id2.TabIndex = 41;
            // 
            // btnAddMeteorite
            // 
            this.btnAddMeteorite.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddMeteorite.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddMeteorite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddMeteorite.Location = new System.Drawing.Point(272, 299);
            this.btnAddMeteorite.Name = "btnAddMeteorite";
            this.btnAddMeteorite.Size = new System.Drawing.Size(196, 48);
            this.btnAddMeteorite.TabIndex = 40;
            this.btnAddMeteorite.Text = "Зберегти ";
            this.btnAddMeteorite.UseVisualStyleBackColor = false;
            this.btnAddMeteorite.Click += new System.EventHandler(this.btnAddMeteorite_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(491, 161);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(99, 96);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(191, 268);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 16);
            this.label26.TabIndex = 53;
            this.label26.Text = "ID простір:";
            // 
            // textBoxMeteorite_ProstirID
            // 
            this.textBoxMeteorite_ProstirID.Location = new System.Drawing.Point(272, 271);
            this.textBoxMeteorite_ProstirID.Name = "textBoxMeteorite_ProstirID";
            this.textBoxMeteorite_ProstirID.Size = new System.Drawing.Size(196, 22);
            this.textBoxMeteorite_ProstirID.TabIndex = 52;
            // 
            // AddMeteorite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.textBoxMeteorite_ProstirID);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_MeteoriteColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_MeteoriteRadius);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_MeteoriteMass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_MeteoriteName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Meteorite_Id2);
            this.Controls.Add(this.btnAddMeteorite);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddMeteorite";
            this.Text = "Метеорити";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_MeteoriteColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_MeteoriteRadius;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_MeteoriteMass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_MeteoriteName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Meteorite_Id2;
        private System.Windows.Forms.Button btnAddMeteorite;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxMeteorite_ProstirID;
    }
}