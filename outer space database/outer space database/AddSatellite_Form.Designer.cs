﻿namespace outer_space_database
{
    partial class AddSatellite_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddSatellite_Form));
            this.btnAddSatellite = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Satallite_Id2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_SatelliteName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_SatelliteMass = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_SatelliteRadius = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_SatelliteColor = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddSatellite
            // 
            this.btnAddSatellite.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddSatellite.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddSatellite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddSatellite.Location = new System.Drawing.Point(234, 268);
            this.btnAddSatellite.Name = "btnAddSatellite";
            this.btnAddSatellite.Size = new System.Drawing.Size(196, 48);
            this.btnAddSatellite.TabIndex = 1;
            this.btnAddSatellite.Text = "Зберегти ";
            this.btnAddSatellite.UseVisualStyleBackColor = false;
            this.btnAddSatellite.Click += new System.EventHandler(this.btnAddSatellite_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "ID:";
            // 
            // textBox_Satallite_Id2
            // 
            this.textBox_Satallite_Id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Satallite_Id2.Location = new System.Drawing.Point(234, 119);
            this.textBox_Satallite_Id2.Name = "textBox_Satallite_Id2";
            this.textBox_Satallite_Id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satallite_Id2.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 25;
            this.label2.Text = "Назва:";
            // 
            // textBox_SatelliteName
            // 
            this.textBox_SatelliteName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_SatelliteName.Location = new System.Drawing.Point(234, 147);
            this.textBox_SatelliteName.Name = "textBox_SatelliteName";
            this.textBox_SatelliteName.Size = new System.Drawing.Size(196, 22);
            this.textBox_SatelliteName.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 27;
            this.label3.Text = "Маса:";
            // 
            // textBox_SatelliteMass
            // 
            this.textBox_SatelliteMass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_SatelliteMass.Location = new System.Drawing.Point(234, 175);
            this.textBox_SatelliteMass.Name = "textBox_SatelliteMass";
            this.textBox_SatelliteMass.Size = new System.Drawing.Size(196, 22);
            this.textBox_SatelliteMass.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 29;
            this.label4.Text = "Радіус:";
            // 
            // textBox_SatelliteRadius
            // 
            this.textBox_SatelliteRadius.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_SatelliteRadius.Location = new System.Drawing.Point(234, 203);
            this.textBox_SatelliteRadius.Name = "textBox_SatelliteRadius";
            this.textBox_SatelliteRadius.Size = new System.Drawing.Size(196, 22);
            this.textBox_SatelliteRadius.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(174, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 31;
            this.label5.Text = "Колір:";
            // 
            // textBox_SatelliteColor
            // 
            this.textBox_SatelliteColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_SatelliteColor.Location = new System.Drawing.Point(234, 231);
            this.textBox_SatelliteColor.Name = "textBox_SatelliteColor";
            this.textBox_SatelliteColor.Size = new System.Drawing.Size(196, 22);
            this.textBox_SatelliteColor.TabIndex = 30;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(495, 106);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(193, 186);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // AddSatellite_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_SatelliteColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_SatelliteRadius);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_SatelliteMass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_SatelliteName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Satallite_Id2);
            this.Controls.Add(this.btnAddSatellite);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddSatellite_Form";
            this.Text = "Супутники";
            this.Load += new System.EventHandler(this.AddSatellite_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddSatellite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Satallite_Id2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_SatelliteName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_SatelliteMass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_SatelliteRadius;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_SatelliteColor;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}