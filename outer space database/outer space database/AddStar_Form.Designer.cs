﻿namespace outer_space_database
{
    partial class AddStar_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddStar_Form));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_StarColor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_StarRadius = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_StarMass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_StarName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Star_Id2 = new System.Windows.Forms.TextBox();
            this.btnAddStar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(405, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(402, 363);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 16);
            this.label5.TabIndex = 50;
            this.label5.Text = "Колір:";
            // 
            // textBox_StarColor
            // 
            this.textBox_StarColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_StarColor.Location = new System.Drawing.Point(203, 245);
            this.textBox_StarColor.Name = "textBox_StarColor";
            this.textBox_StarColor.Size = new System.Drawing.Size(196, 22);
            this.textBox_StarColor.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(143, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 48;
            this.label4.Text = "Радіус:";
            // 
            // textBox_StarRadius
            // 
            this.textBox_StarRadius.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_StarRadius.Location = new System.Drawing.Point(203, 217);
            this.textBox_StarRadius.Name = "textBox_StarRadius";
            this.textBox_StarRadius.Size = new System.Drawing.Size(196, 22);
            this.textBox_StarRadius.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 46;
            this.label3.Text = "Маса:";
            // 
            // textBox_StarMass
            // 
            this.textBox_StarMass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_StarMass.Location = new System.Drawing.Point(203, 189);
            this.textBox_StarMass.Name = "textBox_StarMass";
            this.textBox_StarMass.Size = new System.Drawing.Size(196, 22);
            this.textBox_StarMass.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(145, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "Назва:";
            // 
            // textBox_StarName
            // 
            this.textBox_StarName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_StarName.Location = new System.Drawing.Point(203, 161);
            this.textBox_StarName.Name = "textBox_StarName";
            this.textBox_StarName.Size = new System.Drawing.Size(196, 22);
            this.textBox_StarName.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(173, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 42;
            this.label1.Text = "ID:";
            // 
            // textBox_Star_Id2
            // 
            this.textBox_Star_Id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Star_Id2.Location = new System.Drawing.Point(203, 133);
            this.textBox_Star_Id2.Name = "textBox_Star_Id2";
            this.textBox_Star_Id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Star_Id2.TabIndex = 41;
            // 
            // btnAddStar
            // 
            this.btnAddStar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddStar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddStar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddStar.Location = new System.Drawing.Point(203, 282);
            this.btnAddStar.Name = "btnAddStar";
            this.btnAddStar.Size = new System.Drawing.Size(196, 48);
            this.btnAddStar.TabIndex = 40;
            this.btnAddStar.Text = "Зберегти ";
            this.btnAddStar.UseVisualStyleBackColor = false;
            this.btnAddStar.Click += new System.EventHandler(this.btnAddStar_Click);
            // 
            // AddStar_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox_StarColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_StarRadius);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_StarMass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_StarName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Star_Id2);
            this.Controls.Add(this.btnAddStar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddStar_Form";
            this.Text = "Зірки";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_StarColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_StarRadius;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_StarMass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_StarName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Star_Id2;
        private System.Windows.Forms.Button btnAddStar;
    }
}