﻿namespace outer_space_database
{
    partial class OuterSpace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OuterSpace));
            this.label32 = new System.Windows.Forms.Label();
            this.textBox_IDPROSTIR_OuterSpace = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox_Color_OuterSpace = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox_Radius_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_Mass_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_Name_OuterSpace = new System.Windows.Forms.TextBox();
            this.textBox_SunnySpace_id_OuterSpace = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAddOuterSpace = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(124, 272);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(75, 16);
            this.label32.TabIndex = 35;
            this.label32.Text = "ID простір:";
            // 
            // textBox_IDPROSTIR_OuterSpace
            // 
            this.textBox_IDPROSTIR_OuterSpace.Location = new System.Drawing.Point(206, 266);
            this.textBox_IDPROSTIR_OuterSpace.Name = "textBox_IDPROSTIR_OuterSpace";
            this.textBox_IDPROSTIR_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_IDPROSTIR_OuterSpace.TabIndex = 34;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(154, 241);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 16);
            this.label33.TabIndex = 33;
            this.label33.Text = "Колір:";
            // 
            // textBox_Color_OuterSpace
            // 
            this.textBox_Color_OuterSpace.Location = new System.Drawing.Point(206, 238);
            this.textBox_Color_OuterSpace.Name = "textBox_Color_OuterSpace";
            this.textBox_Color_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color_OuterSpace.TabIndex = 32;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(147, 216);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 16);
            this.label34.TabIndex = 31;
            this.label34.Text = "Радіус:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(156, 185);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 16);
            this.label35.TabIndex = 30;
            this.label35.Text = "Маса:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(147, 157);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 16);
            this.label36.TabIndex = 29;
            this.label36.Text = "Назва:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(176, 129);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(23, 16);
            this.label37.TabIndex = 24;
            this.label37.Text = "ID:";
            // 
            // textBox_Radius_OuterSpace
            // 
            this.textBox_Radius_OuterSpace.Location = new System.Drawing.Point(206, 210);
            this.textBox_Radius_OuterSpace.Name = "textBox_Radius_OuterSpace";
            this.textBox_Radius_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius_OuterSpace.TabIndex = 28;
            // 
            // textBox_Mass_OuterSpace
            // 
            this.textBox_Mass_OuterSpace.Location = new System.Drawing.Point(206, 182);
            this.textBox_Mass_OuterSpace.Name = "textBox_Mass_OuterSpace";
            this.textBox_Mass_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass_OuterSpace.TabIndex = 27;
            // 
            // textBox_Name_OuterSpace
            // 
            this.textBox_Name_OuterSpace.Location = new System.Drawing.Point(206, 154);
            this.textBox_Name_OuterSpace.Name = "textBox_Name_OuterSpace";
            this.textBox_Name_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name_OuterSpace.TabIndex = 26;
            // 
            // textBox_SunnySpace_id_OuterSpace
            // 
            this.textBox_SunnySpace_id_OuterSpace.Location = new System.Drawing.Point(206, 126);
            this.textBox_SunnySpace_id_OuterSpace.Name = "textBox_SunnySpace_id_OuterSpace";
            this.textBox_SunnySpace_id_OuterSpace.Size = new System.Drawing.Size(196, 22);
            this.textBox_SunnySpace_id_OuterSpace.TabIndex = 25;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(443, 51);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 308);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 52;
            this.pictureBox1.TabStop = false;
            // 
            // btnAddOuterSpace
            // 
            this.btnAddOuterSpace.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddOuterSpace.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddOuterSpace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddOuterSpace.Location = new System.Drawing.Point(206, 294);
            this.btnAddOuterSpace.Name = "btnAddOuterSpace";
            this.btnAddOuterSpace.Size = new System.Drawing.Size(196, 48);
            this.btnAddOuterSpace.TabIndex = 53;
            this.btnAddOuterSpace.Text = "Зберегти ";
            this.btnAddOuterSpace.UseVisualStyleBackColor = false;
            this.btnAddOuterSpace.Click += new System.EventHandler(this.btnAddOuterSpace_Click);
            // 
            // OuterSpace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAddOuterSpace);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.textBox_IDPROSTIR_OuterSpace);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.textBox_Color_OuterSpace);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.textBox_Radius_OuterSpace);
            this.Controls.Add(this.textBox_Mass_OuterSpace);
            this.Controls.Add(this.textBox_Name_OuterSpace);
            this.Controls.Add(this.textBox_SunnySpace_id_OuterSpace);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OuterSpace";
            this.Text = "OuterSpace";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox_IDPROSTIR_OuterSpace;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox_Color_OuterSpace;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox_Radius_OuterSpace;
        private System.Windows.Forms.TextBox textBox_Mass_OuterSpace;
        private System.Windows.Forms.TextBox textBox_Name_OuterSpace;
        private System.Windows.Forms.TextBox textBox_SunnySpace_id_OuterSpace;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAddOuterSpace;
    }
}