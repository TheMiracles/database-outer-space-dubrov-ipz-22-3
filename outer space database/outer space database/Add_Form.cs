﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace outer_space_database
{
    public partial class Add_Form : Form
    {
        DataBase dataBase = new DataBase();

        public Add_Form()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataBase.openConnection();

            int Planet_Id;
            var Name = textBox_Name2.Text;
            int Mass;
            int Radius;
            int Distance_sun;
            var Type_planet = textBox_Type_planet2.Text;
            var Color = textBox_Color2.Text;
            int Sun_id;
            int Satallite_id;

            if (int.TryParse(textBox_id2.Text, out Planet_Id) &&
               int.TryParse(textBox_Mass2.Text, out Mass) &&
               int.TryParse(textBox_Radius2.Text, out Radius) &&
               int.TryParse(textBox_Distance_Sun2.Text, out Distance_sun) &&
               int.TryParse(textBox_Sun_id2.Text, out Sun_id) &&
               int.TryParse(textBox_Satellite_id2.Text, out Satallite_id))
            {
                var addQuery = $"insert into Planets (Planet_Id, Name, Mass, Radius, Distance_sun, Type_planet, Color, Sun_id, Satallite_id) values ('{Planet_Id}', '{Name}', '{Mass}', '{Radius}', '{Distance_sun}', '{Type_planet}', '{Color}', '{Sun_id}', '{Satallite_id}')";

                var command = new SqlCommand(addQuery, dataBase.getConnection());
                command.ExecuteNonQuery();

                MessageBox.Show("Запис успішно створено!", "Успіх!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Має бути числовий запис!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            dataBase.closeConnection();   

        }
    }
}
