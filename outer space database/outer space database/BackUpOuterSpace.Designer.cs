﻿namespace outer_space_database
{
    partial class BackUpOuterSpace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackUpOuterSpace));
            this.btnCreateBackUp = new System.Windows.Forms.Button();
            this.textBox_Location = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnBrowse2 = new System.Windows.Forms.Button();
            this.textBox_Location2 = new System.Windows.Forms.TextBox();
            this.btnRestoreBackUp = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCreateBackUp
            // 
            this.btnCreateBackUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCreateBackUp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCreateBackUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreateBackUp.Location = new System.Drawing.Point(548, 161);
            this.btnCreateBackUp.Name = "btnCreateBackUp";
            this.btnCreateBackUp.Size = new System.Drawing.Size(196, 48);
            this.btnCreateBackUp.TabIndex = 55;
            this.btnCreateBackUp.Text = "Створити";
            this.btnCreateBackUp.UseVisualStyleBackColor = false;
            this.btnCreateBackUp.Click += new System.EventHandler(this.btnCreateBackUp_Click);
            // 
            // textBox_Location
            // 
            this.textBox_Location.Location = new System.Drawing.Point(292, 129);
            this.textBox_Location.Multiline = true;
            this.textBox_Location.Name = "textBox_Location";
            this.textBox_Location.Size = new System.Drawing.Size(221, 48);
            this.textBox_Location.TabIndex = 56;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowse.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBrowse.Location = new System.Drawing.Point(548, 104);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(196, 48);
            this.btnBrowse.TabIndex = 57;
            this.btnBrowse.Text = "Вибрати розташування";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnBrowse2
            // 
            this.btnBrowse2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBrowse2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowse2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBrowse2.Location = new System.Drawing.Point(548, 257);
            this.btnBrowse2.Name = "btnBrowse2";
            this.btnBrowse2.Size = new System.Drawing.Size(196, 48);
            this.btnBrowse2.TabIndex = 60;
            this.btnBrowse2.Text = "Вибрати розташування";
            this.btnBrowse2.UseVisualStyleBackColor = false;
            this.btnBrowse2.Click += new System.EventHandler(this.btnBrowse2_Click);
            // 
            // textBox_Location2
            // 
            this.textBox_Location2.Location = new System.Drawing.Point(292, 270);
            this.textBox_Location2.Multiline = true;
            this.textBox_Location2.Name = "textBox_Location2";
            this.textBox_Location2.Size = new System.Drawing.Size(221, 48);
            this.textBox_Location2.TabIndex = 59;
            // 
            // btnRestoreBackUp
            // 
            this.btnRestoreBackUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRestoreBackUp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRestoreBackUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRestoreBackUp.Location = new System.Drawing.Point(548, 314);
            this.btnRestoreBackUp.Name = "btnRestoreBackUp";
            this.btnRestoreBackUp.Size = new System.Drawing.Size(196, 48);
            this.btnRestoreBackUp.TabIndex = 58;
            this.btnRestoreBackUp.Text = "Відновити";
            this.btnRestoreBackUp.UseVisualStyleBackColor = false;
            this.btnRestoreBackUp.Click += new System.EventHandler(this.btnRestoreBackUp_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(49, 117);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(202, 201);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 61;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(545, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 16);
            this.label2.TabIndex = 62;
            this.label2.Text = "Створити Бекап Бази Даних";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(545, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 16);
            this.label1.TabIndex = 63;
            this.label1.Text = "Завантажити Бекап Бази Даних";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 64;
            this.label3.Text = "Шлях файлу";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(360, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 16);
            this.label4.TabIndex = 65;
            this.label4.Text = "Шлях файлу";
            // 
            // BackUpOuterSpace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBrowse2);
            this.Controls.Add(this.textBox_Location2);
            this.Controls.Add(this.btnRestoreBackUp);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.textBox_Location);
            this.Controls.Add(this.btnCreateBackUp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BackUpOuterSpace";
            this.Text = "BackUpOuterSpace";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateBackUp;
        private System.Windows.Forms.TextBox textBox_Location;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnBrowse2;
        private System.Windows.Forms.TextBox textBox_Location2;
        private System.Windows.Forms.Button btnRestoreBackUp;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}