﻿namespace outer_space_database
{
    partial class Add_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Form));
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_Satellite_id2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Sun_id2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_Color2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Type_planet2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Distance_Sun2 = new System.Windows.Forms.TextBox();
            this.textBox_Radius2 = new System.Windows.Forms.TextBox();
            this.textBox_Mass2 = new System.Windows.Forms.TextBox();
            this.textBox_Name2 = new System.Windows.Forms.TextBox();
            this.textBox_id2 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(220, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 48);
            this.button1.TabIndex = 0;
            this.button1.Text = "Зберегти ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(113, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 16);
            this.label9.TabIndex = 37;
            this.label9.Text = "ID Супутників:";
            // 
            // textBox_Satellite_id2
            // 
            this.textBox_Satellite_id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Satellite_id2.Location = new System.Drawing.Point(220, 278);
            this.textBox_Satellite_id2.Name = "textBox_Satellite_id2";
            this.textBox_Satellite_id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Satellite_id2.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(147, 253);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 16);
            this.label8.TabIndex = 35;
            this.label8.Text = "ID Сонця:";
            // 
            // textBox_Sun_id2
            // 
            this.textBox_Sun_id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Sun_id2.Location = new System.Drawing.Point(220, 250);
            this.textBox_Sun_id2.Name = "textBox_Sun_id2";
            this.textBox_Sun_id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Sun_id2.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(168, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 16);
            this.label7.TabIndex = 33;
            this.label7.Text = "Колір:";
            // 
            // textBox_Color2
            // 
            this.textBox_Color2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Color2.Location = new System.Drawing.Point(220, 222);
            this.textBox_Color2.Name = "textBox_Color2";
            this.textBox_Color2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Color2.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(113, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 16);
            this.label6.TabIndex = 31;
            this.label6.Text = "Тип планети:";
            // 
            // textBox_Type_planet2
            // 
            this.textBox_Type_planet2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Type_planet2.Location = new System.Drawing.Point(220, 194);
            this.textBox_Type_planet2.Name = "textBox_Type_planet2";
            this.textBox_Type_planet2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Type_planet2.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 16);
            this.label5.TabIndex = 29;
            this.label5.Text = "Дистанція до сонця:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 28;
            this.label4.Text = "Радіус:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(170, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 27;
            this.label3.Text = "Маса:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(161, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Назва:";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 21;
            this.label1.Text = "ID:";
            // 
            // textBox_Distance_Sun2
            // 
            this.textBox_Distance_Sun2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Distance_Sun2.Location = new System.Drawing.Point(220, 166);
            this.textBox_Distance_Sun2.Name = "textBox_Distance_Sun2";
            this.textBox_Distance_Sun2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Distance_Sun2.TabIndex = 25;
            // 
            // textBox_Radius2
            // 
            this.textBox_Radius2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Radius2.Location = new System.Drawing.Point(220, 138);
            this.textBox_Radius2.Name = "textBox_Radius2";
            this.textBox_Radius2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Radius2.TabIndex = 24;
            // 
            // textBox_Mass2
            // 
            this.textBox_Mass2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Mass2.Location = new System.Drawing.Point(220, 110);
            this.textBox_Mass2.Name = "textBox_Mass2";
            this.textBox_Mass2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Mass2.TabIndex = 23;
            // 
            // textBox_Name2
            // 
            this.textBox_Name2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_Name2.Location = new System.Drawing.Point(220, 82);
            this.textBox_Name2.Name = "textBox_Name2";
            this.textBox_Name2.Size = new System.Drawing.Size(196, 22);
            this.textBox_Name2.TabIndex = 22;
            // 
            // textBox_id2
            // 
            this.textBox_id2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_id2.Location = new System.Drawing.Point(220, 54);
            this.textBox_id2.Name = "textBox_id2";
            this.textBox_id2.Size = new System.Drawing.Size(196, 22);
            this.textBox_id2.TabIndex = 20;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(458, 85);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(244, 234);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // Add_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox_Satellite_id2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox_Sun_id2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_Color2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_Type_planet2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_Distance_Sun2);
            this.Controls.Add(this.textBox_Radius2);
            this.Controls.Add(this.textBox_Mass2);
            this.Controls.Add(this.textBox_Name2);
            this.Controls.Add(this.textBox_id2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Add_Form";
            this.Text = "Планети";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_Satellite_id2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_Sun_id2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_Color2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Type_planet2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Distance_Sun2;
        private System.Windows.Forms.TextBox textBox_Radius2;
        private System.Windows.Forms.TextBox textBox_Mass2;
        private System.Windows.Forms.TextBox textBox_Name2;
        private System.Windows.Forms.TextBox textBox_id2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}