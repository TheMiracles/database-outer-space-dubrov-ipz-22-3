create database solarsystem2
use solarsystem2
create table Sun(
Sun_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Color VARCHAR(50)
)
create table Satellites(
Satallite_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Color VARCHAR(50)
)
create table Planets(
Planet_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Distance_sun BIGINT,
Type_planet VARCHAR(50),
Color VARCHAR(50),
Sun_id int REFERENCES Sun(Sun_id) ON DELETE NO ACTION ON UPDATE CASCADE,
Satallite_id int REFERENCES Satellites(Satallite_id) ON DELETE NO ACTION ON UPDATE CASCADE
)
create table Sunnyspace(
Sunnyspace_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Color VARCHAR(50),
Sun_id int REFERENCES Sun(Sun_id) ON DELETE NO ACTION ON UPDATE CASCADE
)
create table Comets(
Comets_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Color VARCHAR(50),
Sunnyspace_id int REFERENCES Sunnyspace(Sunnyspace_id) ON DELETE NO ACTION ON UPDATE CASCADE
)
create table Meteorites(
Meteorites_id int PRIMARY KEY,
Name VARCHAR(50),
Mass BIGINT,
Radius BIGINT,
Color VARCHAR(50),
Sunnyspace_id int REFERENCES Sunnyspace(Sunnyspace_id) ON DELETE NO ACTION ON UPDATE CASCADE
)
INSERT INTO Sun (Sun_id, Name, Mass, Radius, Color) VALUES 
(1, '�����', 19890, 696340, '�����-���������');
SELECT * FROM Sun;

INSERT INTO Satellites (Satallite_id, Name, Mass, Radius, Color) VALUES
(2, '̳����', 734770, 173710, '����'),
(3, '�����', 134520, 5152, '�����-���������'),
(4, '������', 48000, 3138, '���������-����'),
(5, '�������', 10800, 4821, '���������-��������'),
(6, '������', 21400, 13534, '����������');
SELECT * FROM Satellites;

INSERT INTO Planets (Planet_id, Name, Mass, Radius, Distance_Sun, Type_planet, Color, Sun_id, Satallite_id) VALUES
(7,'�������', 330100, 24397, 69820, '������ ���', '����������', '1', '2'),
(8,'������', 486850, 60518, 108000, '������ ���', '������', '1', '2'),
(9,'�����', 597370, 63710, 14960, '������ ���', '�����', '1', '2'),
(10,'����', 641710, 33895, 250000, '������ ���', '��������', '1', '2'),
(11,'�����', 189860, 690911, 77857, '������� ���', '���������-������', '1', '2'),
(12,'������', 568460, 60268, 143000, '������� ���', '�������', '1', '2'),
(13,'����', 868100, 25559, 287100, '�������� ���', '��������-����', '1', '2'),
(14,'������', 102430, 24764, 450000, '�������� ���', '���������', '1', '2');
SELECT * FROM Planets;

INSERT INTO SunnySpace (SunnySpace_id, Name, Mass, Radius, Color, Sun_id) VALUES 
(15,'������� �������', 1001400, NULL, NULL, '1');
SELECT * FROM SunnySpace;

INSERT INTO Comets (Comets_id, Name, Mass, Radius, Color, Sunnyspace_id) VALUES
(16, '�������� 1', NULL, NULL, NULL, '15'),
(17, '������', NULL, NULL, NULL,'15'),
(18, '������� 1', NULL, NULL, NULL,'15'),
(19, '������� 2', NULL, NULL, NULL,'15'),
(20, '������', 22000 , 120, '����������','15');
SELECT * FROM Comets;

INSERT INTO Meteorites (Meteorites_id, Name, Mass, Radius, Color, Sunnyspace_id) VALUES
(21, '����', 60000, 27, '����������','15'),
(22, '�������', 3000, NULL, '����','15'),
(23, '�������������', 1080, NULL, '������','15'),
(24, 'ѳ����-����', 1000, NULL, '����������','15'),
(25, 'ALH84001', 1930, NULL, '����','15');
SELECT * FROM Meteorites;

Select * from Satellites
Where Satellites.Color = '����������';

Select * from Planets
Where Name Like '�%';

Select * from Satellites
Where Satellites.Color Between '����' And'�����-���������'

Select * from Meteorites
Where Meteorites_id in (21,23)

Select * from Comets
Where Comets.Mass > '1000' And Comets.Name = '������'

SELECT Planets.*, Satellites.Name AS SatelliteName, Satellites.Color AS SatelliteColor
FROM Planets
INNER JOIN Satellites ON Planets.Satallite_id = Satellites.Satallite_id;

SELECT Planets.Color, Satellites.Color
FROM Planets, Satellites
WHERE Planets.Color=Satellites.Color;

SELECT Meteorites.Name
FROM Meteorites
WHERE (Meteorites.Mass>(SELECT AVG (Mass)
FROM Meteorites));

SELECT *
FROM Satellites
WHERE Satallite_id IN (SELECT Satallite_id FROM Satellites
WHERE Mass >=20000);

INSERT INTO Planets( Planet_id, Name, Mass, Radius, Distance_Sun, Type_planet, Color, Sun_id, Satallite_id )
VALUES (26,'������', 13030, 11870, 591000, '���������� ���', '����������', '1', '2');

INSERT INTO Planets (Name)
SELECT Name
FROM Planets
WHERE Name LIKE 'M%';

UPDATE Planets SET Radius = (9.13*Radius)
WHERE Planet_id=9;

DELETE Meteorites
FROM Meteorites
WHERE Meteorites_id=25;

DELETE Planets
FROM Planets
WHERE Color=(SELECT Color FROM Planets
WHERE  Planet_id = 26);

SELECT Name, Color, Mass
FROM Planets
UNION 
SELECT Name, Color, Mass
FROM Satellites;

DECLARE @min DECIMAL(10, 4) = 0.5;
DECLARE @radius_min BIGINT = 1000;
SELECT
    p.Planet_id,
    p.Name AS PlanetName,
    p.Mass AS PlanetMass,
    p.Radius AS PlanetRadius,
    p.Distance_sun AS DistanceFromSun,
    p.Type_planet AS PlanetType,
    p.Color AS PlanetColor,
    s.Satallite_id AS SatelliteID,
    s.Name AS SatelliteName,
    s.Mass AS SatelliteMass,
    s.Radius AS SatelliteRadius,
    s.Color AS SatelliteColor
FROM
    Planets p
INNER JOIN
    Satellites s ON p.Satallite_id = s.Satallite_id
WHERE
    p.Mass > @min
    AND s.Radius >= @radius_min;

CREATE VIEW Sunnyspace_Comets_View AS
SELECT
  s.Sunnyspace_id,
  s.Name AS Sunnyspace_Name,
  s.Mass AS Sunnyspace_Mass,
  s.Radius AS Sunnyspace_Radius,
  s.Color AS Sunnyspace_Color,
  c.Comets_id,
  c.Name AS Comet_Name,
  c.Mass AS Comet_Mass,
  c.Radius AS Comet_Radius,
  c.Color AS Comet_Color
FROM Sunnyspace s
LEFT JOIN Comets c ON s.Sunnyspace_id = c.Sunnyspace_id;
SELECT * FROM Sunnyspace_Comets_View;


CREATE VIEW Sunnyspace_Meteorites_View AS
SELECT
  s.Sunnyspace_id,
  s.Name AS Sunnyspace_Name,
  s.Mass AS Sunnyspace_Mass,
  s.Radius AS Sunnyspace_Radius,
  s.Color AS Sunnyspace_Color,
  m.Meteorites_id,
  m.Name AS Meteorite_Name,
  m.Mass AS Meteorite_Mass,
  m.Radius AS Meteorite_Radius,
  m.Color AS Meteorite_Color
FROM Sunnyspace s
LEFT JOIN Meteorites m ON s.Sunnyspace_id = m.Sunnyspace_id;
SELECT * FROM Sunnyspace_Meteorites_View;


CREATE VIEW Sun_Planets_View AS
SELECT
  s.Sun_id,
  s.Name AS Sun_Name,
  s.Mass AS Sun_Mass,
  s.Radius AS Sun_Radius,
  s.Color AS Sun_Color,
  p.Planet_id,
  p.Name AS Planet_Name,
  p.Mass AS Planet_Mass,
  p.Radius AS Planet_Radius,
  p.Distance_sun AS Planet_Distance_Sun,
  p.Type_planet AS Planet_Type,
  p.Color AS Planet_Color
FROM Sun s
LEFT JOIN Planets p ON s.Sun_id = p.Sun_id;
SELECT * FROM Sun_Planets_View;


CREATE VIEW Sun_Satellites_View AS
SELECT
  s.Sun_id,
  s.Name AS Sun_Name,
  s.Mass AS Sun_Mass,
  s.Radius AS Sun_Radius,
  s.Color AS Sun_Color,
  sat.Satallite_id,
  sat.Name AS Satellite_Name,
  sat.Mass AS Satellite_Mass,
  sat.Radius AS Satellite_Radius,
  sat.Color AS Satellite_Color
FROM Sun s
LEFT JOIN Satellites sat ON s.Sun_id = sat.Satallite_id;
SELECT * FROM Sun_Satellites_View;


CREATE VIEW Sunnyspace_Objects_View AS
SELECT
  s.Sunnyspace_id,
  s.Name AS Sunnyspace_Name,
  s.Mass AS Sunnyspace_Mass,
  s.Radius AS Sunnyspace_Radius,
  s.Color AS Sunnyspace_Color,
  c.Comets_id,
  c.Name AS Comet_Name,
  c.Mass AS Comet_Mass,
  c.Radius AS Comet_Radius,
  c.Color AS Comet_Color,
  m.Meteorites_id,
  m.Name AS Meteorite_Name,
  m.Mass AS Meteorite_Mass,
  m.Radius AS Meteorite_Radius,
  m.Color AS Meteorite_Color
FROM Sunnyspace s
LEFT JOIN Comets c ON s.Sunnyspace_id = c.Sunnyspace_id
LEFT JOIN Meteorites m ON s.Sunnyspace_id = m.Sunnyspace_id;
SELECT * FROM Sunnyspace_Objects_View;

CREATE PROCEDURE GetCelestialBodiesAboveEarth AS
BEGIN
    SELECT Sun.Name, Sun.Mass FROM Sun WHERE Sun.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
    SELECT Satellites.Name, Satellites.Mass FROM Satellites WHERE Satellites.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
    SELECT Planets.Name, Planets.Mass FROM Planets WHERE Planets.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
    SELECT Sunnyspace.Name, Sunnyspace.Mass FROM Sunnyspace WHERE Sunnyspace.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
    SELECT Comets.Name, Comets.Mass FROM Comets WHERE Comets.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
    SELECT Meteorites.Name, Meteorites.Mass FROM Meteorites WHERE Meteorites.Mass > (SELECT Mass FROM Planets WHERE Name = '�����');
END;
Execute GetCelestialBodiesAboveEarth

CREATE PROCEDURE ReduceSaturnRadius AS
BEGIN
    UPDATE Planets
    SET Radius = Radius * 0.9
    WHERE Planets.Name = '������';
END;
Execute ReduceSaturnRadius

CREATE PROCEDURE GetBrownColoredBodies AS
BEGIN
    SELECT Name, Color FROM Sun WHERE Color = '����������';
    SELECT Name, Color FROM Satellites WHERE Color = '����������';
    SELECT Name, Color FROM Planets WHERE Color = '����������';
    SELECT Name, Color FROM Sunnyspace WHERE Color = '����������';
    SELECT Name, Color FROM Comets WHERE Color = '����������';
    SELECT Name, Color FROM Meteorites WHERE Color = '����������';
END;
Execute GetBrownColoredBodies

CREATE PROCEDURE CountBrownMeteorites AS
BEGIN
    SELECT COUNT(*) AS BrownMeteoriteCount FROM Meteorites WHERE Color = '����������';
END;
Execute CountBrownMeteorites

CREATE PROCEDURE CountWhiteSatellites AS
BEGIN
    SELECT COUNT(*) AS WhiteSatelliteCount
    FROM Satellites
    INNER JOIN Planets ON Satellites.Satallite_id = Planets.Satallite_id
    WHERE Satellites.Color = '����';
END;
Execute CountWhiteSatellites

CREATE PROCEDURE overlay
  @name VARCHAR(255),
  @mass BIGINT,
  @radius BIGINT,
  @color VARCHAR(50),
  @Sun_id INT
AS
BEGIN
  DECLARE @countSubject INT;
  SET NOCOUNT ON;
  SELECT @countSubject = COUNT(*) FROM Planets;
  INSERT INTO Planets (Name, Mass, Radius, Color, Sun_id)
  VALUES (@name, @mass, @radius, @color, @Sun_id);
END;

DECLARE @countSubject INT;
EXEC overlay '�����' , '40000' , '40000' , '���������' , '1' , @countSubject output
PRINT '����� �������: ' + CONVERT(VARCHAR, @countSubject);

CREATE TRIGGER UpdatePlanetOnUpdateSatellite
ON Satellites
AFTER UPDATE
AS
BEGIN
    UPDATE Planets
    SET Planets.Mass = Planets.Mass + (INSERTED.Mass - DELETED.Mass),
        Planets.Radius = Planets.Radius + (INSERTED.Radius - DELETED.Radius)
    FROM Planets
    INNER JOIN INSERTED ON Planets.Planet_id = INSERTED.Satallite_id
    INNER JOIN DELETED ON Planets.Planet_id = DELETED.Satallite_id;
END;

CREATE TRIGGER UpdateSunnySpaceOnInsertComet
ON Comets
AFTER INSERT
AS
BEGIN
    UPDATE SunnySpace
    SET Mass = Mass + (SELECT SUM(Mass) FROM inserted),
        Radius = Radius + (SELECT SUM(Radius) FROM inserted)
    WHERE Sunnyspace_id = (SELECT Sunnyspace_id FROM inserted);
END;

CREATE TRIGGER UpdateSunnySpaceMassMeteorites
ON Meteorites
AFTER INSERT
AS
BEGIN
  UPDATE s
  SET Mass = s.Mass + i.Mass
  FROM SunnySpace s
  INNER JOIN inserted i ON s.Sunnyspace_id = i.Sunnyspace_id;
END;

create table register(
id_user int identity(1,1) not null,
login_user varchar(50) not null,
password_user varchar(50) not null,
is_admin bit
);

insert into register (login_user, password_user,is_admin) values ('admin','admin', 1)

select * from register